using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Card: MonoBehaviour
{

	//	SpriteRenderer spriterenderer;
	Image image;

	public Sprite[] faces;
	public Sprite cardBack;

	//	public Image[] faces;
	//	public Image cardBack;

	public int cardIndex;
	int cardValue;

	//	enum Suit
	//	{
	//		corazones,
	//		picas,
	//		rombos,
	//		treboles
	//	}

	Canvas canvas;



	public int CardIndex { get; set ; }

	public int CardValue { get; set ; }


	void Awake ()
	{

		canvas = (Canvas)GameObject.FindObjectOfType (typeof(Canvas));
		transform.SetParent (canvas.transform);
		image = GetComponent<Image> ();
		print ("inicio cardIndex " + cardIndex);
	}


	public int AssignValue (int cardIndex)
	{

		cardValue = cardIndex % 13;
		cardValue = cardValue + 1;

		if (cardValue == 1) {
			print ("es un as");
		}

		if (cardValue < 1) {
			print ("SALIO 0");
		}
			
		return cardValue;
	}

	public void ToggleCard (bool showFace)
	{
		if (showFace) {
			image.sprite = faces [cardIndex];
//			GetComponent<SpriteRenderer> ().sprite = faces [cardIndex];
		} else {
			
			image.sprite = cardBack;
//			GetComponent<SpriteRenderer> ().sprite = cardBack;

		}

	}



}
