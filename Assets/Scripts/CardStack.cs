﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class CardStack : MonoBehaviour
{
	List<int> cards;

	public bool isGameDeck;


	void Awake ()
	{
		cards = new List<int> ();
		if (isGameDeck) {
			CreateDeck ();
		}
	}

	public int CardCount {
		get {
			if (cards == null) {
				return 0;
			} else {
				return cards.Count;
			}
		}
	}


	public IEnumerable<int>GetCards ()
	{
		foreach (int i in cards) {
			yield return i;
		}
	}

	public bool HasCards {
		get{ return cards != null && cards.Count > 0; }
	}

	public event CardEventHandler CardRemoved;
	public event CardEventHandler CardAdded;

	
	public int Pop ()
	{
		int temp = cards [0];
		cards.RemoveAt (0);
		if (CardRemoved != null) {
			CardRemoved (this, new CardEventArgs (temp));
		}
		return temp;
	}

	public void RemoveCard (int card)
	{
//		cards.RemoveAt (cards.IndexOf (card));
		if (CardRemoved != null) {
			CardRemoved (this, new CardEventArgs (card));
		}
	}

	public void Push (int card)
	{
		cards.Add (card);

		if (CardAdded != null) {
			CardAdded (this, new CardEventArgs (card));
		} 
	}

	public void CreateDeck ()
	{

		cards.Clear ();
		

		for (int i = 0; i < 52; i++) {
			cards.Add (i);
		}

		int n = cards.Count;

		while (n > 1) {
			n--;
			int k = Random.Range (0, n + 1);
			int temp = cards [k];
			cards [k] = cards [n];
			cards [n] = temp;

		}

	}

	public void Reset ()
	{
		cards.Clear ();
	}

}
