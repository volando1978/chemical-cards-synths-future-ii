﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using UnityEngine.UI;

[RequireComponent (typeof(CardStack))]
public class CardStackView : MonoBehaviour
{

	CardStack deck;
	//	public Vector3 startPos;
	public bool faceUp = false;
	public bool reverseLayerOrder = false;
	public GameObject cardPrefab;

	Dictionary<int,CardView> fetchedCards;
	int lastCount;
	float cardOffset = 1;


	// Use this for initialization
	void Awake ()
	{
		fetchedCards = new Dictionary<int,CardView> ();
		deck = GetComponent<CardStack> ();
		ShowCards ();
		lastCount = deck.CardCount;

		deck.CardRemoved += deck_CardRemoved;
		deck.CardAdded += deck_CardAdded;
		cardOffset = Screen.width / 4;

	}

	public Vector3 WorldToGuiPoint (Vector3 position)
	{
		var guiPosition = Camera.main.WorldToScreenPoint (position);
		guiPosition.y = Screen.height - guiPosition.y;
	
		return guiPosition;
	}

	public void Toggle (int card, bool isFaceUp)
	{
		fetchedCards [card].IsFaceUp = isFaceUp;
	}


	public void Flipper (int card)
	{
		fetchedCards [card].FlipCard (fetchedCards [card].Card, card);
	}

	public void Clear ()
	{

		deck.Reset ();

		foreach (CardView view in fetchedCards.Values) {
			Destroy (view.Card);
		}

		fetchedCards.Clear ();
	}


	void deck_CardRemoved (object sender, CardEventArgs e)
	{
		print ("destroying " + e.CardIndex);
		if (fetchedCards.ContainsKey (e.CardIndex)) {
			Destroy (fetchedCards [e.CardIndex].Card);
			fetchedCards.Remove (e.CardIndex);
		}
//		RemoveCard (e.CardIndex, deck.CardCount);
	}

	void deck_CardAdded (object sender, CardEventArgs e)
	{
		print ("card added");
		float co = cardOffset * deck.CardCount;
		Vector3 temp = transform.position + new Vector3 (co, 0f);//startPos + new Vector3 (co, 0f);//
		print ("temp:" + temp + "tp " + transform.position + " " + deck.CardCount);
		AddCard (temp, e.CardIndex, deck.CardCount); 
	}


	void Update ()
	{
		if (lastCount != deck.CardCount) {
			lastCount = deck.CardCount;
			ShowCards ();
		}
	}
	
	// Update is called once per frame
	public void ShowCards ()
	{
	
		int cardCount = 0;

		if (deck.HasCards) {

			foreach (int i in deck.GetCards()) {

				float co = cardCount * 0.2f;
				Vector3 temp = transform.position + new Vector3 (0f, co);//startPos + new Vector3 (co, 0f);
				AddCard (temp, i, cardCount);
				cardCount++;

			}
		}
	}

	//	void RemoveCard (int cardIndex, int positionalIndex)
	//	{
	//		if (fetchedCards.ContainsKey (cardIndex)) {
	//			fetchedCards.Remove (cardIndex);
	//
	//
	//		}
	//	}


	void AddCard (Vector3 position, int cardIndex, int positionalIndex)
	{

		if (fetchedCards.ContainsKey (cardIndex)) {

			
			if (!faceUp) {
				Card model = fetchedCards [cardIndex].Card.GetComponent<Card> ();
				model.ToggleCard (fetchedCards [cardIndex].IsFaceUp);
			}

			return;
		}

		GameObject cardCopy = Instantiate (cardPrefab) as GameObject;
//		GameObject cardCopy = Instantiate (cardPrefab, cardPrefab.transform.position, cardPrefab.transform.rotation) as GameObject;

		cardCopy.transform.SetParent (gameObject.transform, false);
		cardCopy.GetComponent<RectTransform> ().position = position;//WorldToGuiPoint (position); //new Vector3 (position.x, position.y, 0f);
		cardCopy.transform.localScale = Vector3.one;


		Card cardModel = cardCopy.GetComponent<Card> ();
		cardModel.cardIndex = cardIndex;
		cardModel.CardValue = cardModel.AssignValue (cardIndex);
		cardModel.ToggleCard (faceUp);

		SpriteRenderer spriteRenderer = cardCopy.GetComponent<SpriteRenderer> ();
		if (reverseLayerOrder) {

			spriteRenderer.sortingOrder = 51 - positionalIndex;

		} else {

			spriteRenderer.sortingOrder = positionalIndex;
		}

		fetchedCards.Add (cardIndex, new CardView (cardCopy));

//		Debug.Log ("Card Value: " + deck.AssignValue ());

	}



}
