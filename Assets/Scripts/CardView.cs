﻿using UnityEngine;
//using System.Collections;

public class CardView
{
	public GameObject Card { get; private set; }
	public bool IsFaceUp { get; set; }



	public CardView (GameObject card)
	{
		Card = card;
		IsFaceUp = false;
	}

	public void FlipCard (GameObject card, int cardIndex)
	{
		Card cardModel = card.GetComponent<Card> ();
		card.GetComponent<CardFlipper> ().FlipCard (cardModel.cardBack, cardModel.faces [cardIndex], cardIndex);
	}
}
