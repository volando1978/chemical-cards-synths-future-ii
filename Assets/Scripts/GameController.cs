﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

	int dealersFirstCard = -1;

	public CardStack dealer;
	public CardStack deck;
	public CardStack player;


	public Button hitButton;
	public Button stickButton;
	public Button playAgainButton;

	public Text winnerText;

	/*
	 * Cards dealt to each player
	 * First player hits/sticks / bust
	 * Dealer's turn; must have minimum of 17 score hand
	 * Dealers cards; first card is hidden, subsequent cards are facing
	 */

	#region Public Methods

	public void Hit ()
	{
//		player.PushClassic (deck.Pop ());
//		if (player.HandValue () > 21) {
//			//player bust
//			hitButton.interactable = false;
//			stickButton.interactable = false;
//			StartCoroutine (DealersTurn ());
//
//		}
	}

	public void Stick ()
	{
		hitButton.interactable = false;
		stickButton.interactable = false;

		StartCoroutine (DealersTurn ());
	}

	public void PlayAgain ()
	{
		playAgainButton.interactable = false;

		deck.GetComponent<CardStackView> ().Clear ();
		player.GetComponent<CardStackView> ().Clear ();
		dealer.GetComponent<CardStackView> ().Clear ();

		winnerText.text = "";

		deck.CreateDeck ();
		dealersFirstCard = -1;

		hitButton.interactable = true;
		stickButton.interactable = true;

		StartGame ();

	}

	#endregion



	void Awake ()
	{
		StartGame ();
	}


	void StartGame ()
	{

		for (int i = 0; i < 2; i++) {
			player.Push (deck.Pop ());
			HitDealer ();
		}

	}

	//ask the dealer for another card
	void HitDealer ()
	{
		int card = deck.Pop ();

		if (dealersFirstCard < 0) {
			dealersFirstCard = card; 
		}

		dealer.Push (card);

		if (dealer.CardCount <= 2) {
			CardStackView view = dealer.GetComponent<CardStackView> ();
			view.Toggle (card, true);
		}
	}

	IEnumerator DealersTurn ()
	{
//		CardStackView view = dealer.GetComponent<CardStackView> ();
//		view.Toggle (dealersFirstCard, true);
//		view.ShowCards ();
//		yield return new WaitForSeconds (1f);
//
//		while (dealer.HandValue () < 17) {
//			HitDealer ();
//
//			yield return new WaitForSeconds (1f);
//		}
//
//		if (player.HandValue () < 21 || (dealer.HandValue () >= player.HandValue () && dealer.HandValue () <= 21)) {
//			winnerText.text = "Sorry, you lose";
//		} else if (dealer.HandValue () > 21 || (player.HandValue () <= 21 && player.HandValue () > dealer.HandValue ())) {
//			winnerText.text = " Winner!";
//		} else {
//			winnerText.text = " The house wins";
//		}
		yield return new WaitForSeconds (1f);
//		playAgainButton.interactable = true;
//
//
	}
}
