﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LifeHud : MonoBehaviour
{

	Text text;



	void Awake ()
	{
		text = GetComponent<Text> ();
//		text.text = "LIFE " + myGameController.instance.Life;
		myGameController.instance.UpdateLifeHud += myGameController_UpdateLife;

	}

	void myGameController_UpdateLife (object sender, UpdateLifeHudEventArgs e)
	{
		text.text = "LIFE " + e.Life;

	}



}
