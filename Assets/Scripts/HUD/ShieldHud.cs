﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShieldHud : MonoBehaviour
{

	Text text;

	void Awake ()
	{
		text = GetComponent<Text> ();
//		text.text = "SHIELD " + myGameController.instance.Shield;
		myGameController.instance.UpdateShieldHud += myGameController_UpdateShield;

	}

	void myGameController_UpdateShield (object sender, UpdateShieldHudEventArgs e)
	{
		text.text = "SHIELD " + e.Shield;

	}



}
