﻿using System;

public class UpdateLifeHudEventArgs : EventArgs
{
	public int Life { get; private set; }

	public UpdateLifeHudEventArgs (int life)
	{
		Life = life;
	}
		

}
