﻿using UnityEngine;
using System;

public class UpdateShieldHudEventArgs : EventArgs
{
	public int Shield { get; private set; }

	public UpdateShieldHudEventArgs (int shield)
	{
		Shield = shield;
	}


}
