﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class myGameController : MonoBehaviour
{


	public CardStack deck;
	public CardStack dealer;
	public CardStack player;

	int life;
	int shield;

	public int Life { get; set; }

	public int Shield { get; set; }

	public event UpdateShieldHudHandler UpdateShieldHud;
	public event UpdateLifeHudHandler UpdateLifeHud;

	public static myGameController instance = null;

	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);    
	}

	// Use this for initialization
	void Start ()
	{
		StartGame ();
	}


	void StartGame ()
	{
		DealCards ();
		life = 21;
//		shield = Random.Range (1, 22);

//		if (UpdateLifeHud != null)
		UpdateLifeHud (this, new UpdateLifeHudEventArgs (life));

		if (UpdateShieldHud != null)
			UpdateShieldHud (this, new UpdateShieldHudEventArgs (shield));

	}

	void DealCards ()
	{
		print (dealer.CardCount);

		if (dealer.CardCount > 0) {
			return;
		}
		StartCoroutine (DealerGivesCards ());
	}

	IEnumerator DealerGivesCards ()
	{
//reparte 3 cartas
		for (int i = 0; i < 4; i++) {
			int card = deck.Pop ();
			dealer.Push (card);
			CardStackView view = dealer.GetComponent<CardStackView> ();
			view.Toggle (card, true);
			view.Flipper (card);

//			chemicalButton.interactable = true;
//			synthButton.interactable = true;
//			futuroButton.interactable = true;
//			discardButton.interactable = true;

			yield return new WaitForSeconds (1f);
		}

	}

	#region Public Methods

	public void HitCount (int card, int cardIndex)
	{

		//logica
		life -= card;
		print (card + "  index " + cardIndex);
		dealer.RemoveCard (card);


		shield = Random.Range (1, 22);

		if (UpdateLifeHud != null)
			UpdateLifeHud (this, new UpdateLifeHudEventArgs (life));

		if (UpdateShieldHud != null)
			UpdateShieldHud (this, new UpdateShieldHudEventArgs (shield));

		if (life < 1) {
			RestartGame ();
		}
		print ("Life: " + life);
	}

	void RestartGame ()
	{
		StartGame ();
	}


	#endregion
}
