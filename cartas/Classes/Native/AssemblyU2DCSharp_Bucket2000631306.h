﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3644373756;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// Bucket
struct  Bucket_t2000631306  : public MonoBehaviour_t3012272455
{
	// UnityEngine.GameObject Bucket::player
	GameObject_t4012695102 * ___player_2;
	// UnityEngine.GameObject Bucket::dealer
	GameObject_t4012695102 * ___dealer_3;
	// System.Collections.Generic.List`1<System.Int32> Bucket::listaQ
	List_1_t3644373756 * ___listaQ_4;
	// System.Collections.Generic.List`1<System.Int32> Bucket::listaS
	List_1_t3644373756 * ___listaS_5;
	// System.Collections.Generic.List`1<System.Int32> Bucket::listaF
	List_1_t3644373756 * ___listaF_6;
};
