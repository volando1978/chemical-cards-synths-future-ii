﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Bucket
struct Bucket_t2000631306;

#include "codegen/il2cpp-codegen.h"

// System.Void Bucket::.ctor()
extern "C"  void Bucket__ctor_m2493733329 (Bucket_t2000631306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket::Start()
extern "C"  void Bucket_Start_m1440871121 (Bucket_t2000631306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket::CreateBucket()
extern "C"  void Bucket_CreateBucket_m3162219481 (Bucket_t2000631306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket::PushQ(System.Int32)
extern "C"  void Bucket_PushQ_m652156407 (Bucket_t2000631306 * __this, int32_t ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket::PushS(System.Int32)
extern "C"  void Bucket_PushS_m3667260025 (Bucket_t2000631306 * __this, int32_t ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Bucket::PushF(System.Int32)
extern "C"  void Bucket_PushF_m1248955692 (Bucket_t2000631306 * __this, int32_t ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
