﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Image
struct Image_t3354615620;
// UnityEngine.Sprite[]
struct SpriteU5BU5D_t503173063;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// UnityEngine.Canvas
struct Canvas_t3534013893;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// Card
struct  Card_t2092848  : public MonoBehaviour_t3012272455
{
	// UnityEngine.UI.Image Card::image
	Image_t3354615620 * ___image_2;
	// UnityEngine.Sprite[] Card::faces
	SpriteU5BU5D_t503173063* ___faces_3;
	// UnityEngine.Sprite Card::cardBack
	Sprite_t4006040370 * ___cardBack_4;
	// System.Int32 Card::cardIndex
	int32_t ___cardIndex_5;
	// System.Int32 Card::cardValue
	int32_t ___cardValue_6;
	// UnityEngine.Canvas Card::canvas
	Canvas_t3534013893 * ___canvas_7;
	// System.Int32 Card::<CardIndex>k__BackingField
	int32_t ___U3CCardIndexU3Ek__BackingField_8;
	// System.Int32 Card::<CardValue>k__BackingField
	int32_t ___U3CCardValueU3Ek__BackingField_9;
};
