﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Card
struct Card_t2092848;

#include "codegen/il2cpp-codegen.h"

// System.Void Card::.ctor()
extern "C"  void Card__ctor_m245477739 (Card_t2092848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Card::get_CardIndex()
extern "C"  int32_t Card_get_CardIndex_m542160116 (Card_t2092848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::set_CardIndex(System.Int32)
extern "C"  void Card_set_CardIndex_m3891891615 (Card_t2092848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Card::get_CardValue()
extern "C"  int32_t Card_get_CardValue_m3125440979 (Card_t2092848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::set_CardValue(System.Int32)
extern "C"  void Card_set_CardValue_m1364980734 (Card_t2092848 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::Awake()
extern "C"  void Card_Awake_m483082958 (Card_t2092848 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Card::AssignValue(System.Int32)
extern "C"  int32_t Card_AssignValue_m3124969166 (Card_t2092848 * __this, int32_t ___cardIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Card::ToggleCard(System.Boolean)
extern "C"  void Card_ToggleCard_m309718708 (Card_t2092848 * __this, bool ___showFace, const MethodInfo* method) IL2CPP_METHOD_ATTR;
