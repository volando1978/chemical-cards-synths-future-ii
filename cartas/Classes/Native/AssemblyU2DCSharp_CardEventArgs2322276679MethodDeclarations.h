﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardEventArgs
struct CardEventArgs_t2322276679;

#include "codegen/il2cpp-codegen.h"

// System.Void CardEventArgs::.ctor(System.Int32)
extern "C"  void CardEventArgs__ctor_m1327271765 (CardEventArgs_t2322276679 * __this, int32_t ___cardIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CardEventArgs::get_CardIndex()
extern "C"  int32_t CardEventArgs_get_CardIndex_m3530186089 (CardEventArgs_t2322276679 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardEventArgs::set_CardIndex(System.Int32)
extern "C"  void CardEventArgs_set_CardIndex_m3495056824 (CardEventArgs_t2322276679 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
