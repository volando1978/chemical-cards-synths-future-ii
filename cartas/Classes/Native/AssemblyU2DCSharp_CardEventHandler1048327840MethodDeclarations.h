﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardEventHandler
struct CardEventHandler_t1048327840;
// System.Object
struct Object_t;
// CardEventArgs
struct CardEventArgs_t2322276679;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_CardEventArgs2322276679.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void CardEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CardEventHandler__ctor_m1251021639 (CardEventHandler_t1048327840 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardEventHandler::Invoke(System.Object,CardEventArgs)
extern "C"  void CardEventHandler_Invoke_m524776984 (CardEventHandler_t1048327840 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_CardEventHandler_t1048327840(Il2CppObject* delegate, Object_t * ___sender, CardEventArgs_t2322276679 * ___e);
// System.IAsyncResult CardEventHandler::BeginInvoke(System.Object,CardEventArgs,System.AsyncCallback,System.Object)
extern "C"  Object_t * CardEventHandler_BeginInvoke_m1222706639 (CardEventHandler_t1048327840 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, AsyncCallback_t1363551830 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CardEventHandler_EndInvoke_m579289303 (CardEventHandler_t1048327840 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
