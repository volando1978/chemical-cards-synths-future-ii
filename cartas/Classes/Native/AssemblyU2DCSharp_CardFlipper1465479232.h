﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// Card
struct Card_t2092848;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3342907448;
struct AnimationCurve_t3342907448_marshaled;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// CardFlipper
struct  CardFlipper_t1465479232  : public MonoBehaviour_t3012272455
{
	// UnityEngine.SpriteRenderer CardFlipper::spriterenderer
	SpriteRenderer_t2223784725 * ___spriterenderer_2;
	// Card CardFlipper::cardModel
	Card_t2092848 * ___cardModel_3;
	// UnityEngine.AnimationCurve CardFlipper::scaleCurve
	AnimationCurve_t3342907448 * ___scaleCurve_4;
	// System.Single CardFlipper::duration
	float ___duration_5;
};
