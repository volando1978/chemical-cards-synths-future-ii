﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardFlipper
struct CardFlipper_t1465479232;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"

// System.Void CardFlipper::.ctor()
extern "C"  void CardFlipper__ctor_m1674335019 (CardFlipper_t1465479232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardFlipper::Awake()
extern "C"  void CardFlipper_Awake_m1911940238 (CardFlipper_t1465479232 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardFlipper::FlipCard(UnityEngine.Sprite,UnityEngine.Sprite,System.Int32)
extern "C"  void CardFlipper_FlipCard_m3019275047 (CardFlipper_t1465479232 * __this, Sprite_t4006040370 * ___startImage, Sprite_t4006040370 * ___endImage, int32_t ___cardIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CardFlipper::Flip(UnityEngine.Sprite,UnityEngine.Sprite,System.Int32)
extern "C"  Object_t * CardFlipper_Flip_m3737881903 (CardFlipper_t1465479232 * __this, Sprite_t4006040370 * ___startImage, Sprite_t4006040370 * ___endImage, int32_t ___cardIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
