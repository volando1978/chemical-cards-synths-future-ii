﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object837106420.h"

// CardFlipper/<Flip>c__Iterator0
struct  U3CFlipU3Ec__Iterator0_t2620986452  : public Object_t
{
	// System.Int32 CardFlipper/<Flip>c__Iterator0::$PC
	int32_t ___U24PC_0;
	// System.Object CardFlipper/<Flip>c__Iterator0::$current
	Object_t * ___U24current_1;
};
