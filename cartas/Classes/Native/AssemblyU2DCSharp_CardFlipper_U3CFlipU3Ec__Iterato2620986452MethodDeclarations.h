﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardFlipper/<Flip>c__Iterator0
struct U3CFlipU3Ec__Iterator0_t2620986452;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CardFlipper/<Flip>c__Iterator0::.ctor()
extern "C"  void U3CFlipU3Ec__Iterator0__ctor_m503058808 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CardFlipper/<Flip>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Object_t * U3CFlipU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2884500644 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CardFlipper/<Flip>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Object_t * U3CFlipU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4099262520 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardFlipper/<Flip>c__Iterator0::MoveNext()
extern "C"  bool U3CFlipU3Ec__Iterator0_MoveNext_m3402620516 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardFlipper/<Flip>c__Iterator0::Dispose()
extern "C"  void U3CFlipU3Ec__Iterator0_Dispose_m2301460405 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardFlipper/<Flip>c__Iterator0::Reset()
extern "C"  void U3CFlipU3Ec__Iterator0_Reset_m2444459045 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
