﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t3644373756;
// CardEventHandler
struct CardEventHandler_t1048327840;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// CardStack
struct  CardStack_t1747837240  : public MonoBehaviour_t3012272455
{
	// System.Collections.Generic.List`1<System.Int32> CardStack::cards
	List_1_t3644373756 * ___cards_2;
	// System.Boolean CardStack::isGameDeck
	bool ___isGameDeck_3;
	// CardEventHandler CardStack::CardRemoved
	CardEventHandler_t1048327840 * ___CardRemoved_4;
	// CardEventHandler CardStack::CardAdded
	CardEventHandler_t1048327840 * ___CardAdded_5;
};
