﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardStack
struct CardStack_t1747837240;
// CardEventHandler
struct CardEventHandler_t1048327840;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CardEventHandler1048327840.h"

// System.Void CardStack::.ctor()
extern "C"  void CardStack__ctor_m4094366515 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::add_CardRemoved(CardEventHandler)
extern "C"  void CardStack_add_CardRemoved_m4115931587 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::remove_CardRemoved(CardEventHandler)
extern "C"  void CardStack_remove_CardRemoved_m4014251398 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::add_CardAdded(CardEventHandler)
extern "C"  void CardStack_add_CardAdded_m219470563 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::remove_CardAdded(CardEventHandler)
extern "C"  void CardStack_remove_CardAdded_m3513215846 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::Awake()
extern "C"  void CardStack_Awake_m37004438 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CardStack::get_CardCount()
extern "C"  int32_t CardStack_get_CardCount_m2692224405 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Int32> CardStack::GetCards()
extern "C"  Object_t* CardStack_GetCards_m3171586759 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardStack::get_HasCards()
extern "C"  bool CardStack_get_HasCards_m3672825519 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CardStack::Pop()
extern "C"  int32_t CardStack_Pop_m3559568496 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::RemoveCard(System.Int32)
extern "C"  void CardStack_RemoveCard_m3746222646 (CardStack_t1747837240 * __this, int32_t ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::Push(System.Int32)
extern "C"  void CardStack_Push_m1325389084 (CardStack_t1747837240 * __this, int32_t ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::CreateDeck()
extern "C"  void CardStack_CreateDeck_m726432310 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack::Reset()
extern "C"  void CardStack_Reset_m1740799456 (CardStack_t1747837240 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
