﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardStack
struct CardStack_t1747837240;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// System.Collections.Generic.Dictionary`2<System.Int32,CardView>
struct Dictionary_2_t547271572;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// CardStackView
struct  CardStackView_t4019401725  : public MonoBehaviour_t3012272455
{
	// CardStack CardStackView::deck
	CardStack_t1747837240 * ___deck_2;
	// System.Boolean CardStackView::faceUp
	bool ___faceUp_3;
	// System.Boolean CardStackView::reverseLayerOrder
	bool ___reverseLayerOrder_4;
	// UnityEngine.GameObject CardStackView::cardPrefab
	GameObject_t4012695102 * ___cardPrefab_5;
	// System.Collections.Generic.Dictionary`2<System.Int32,CardView> CardStackView::fetchedCards
	Dictionary_2_t547271572 * ___fetchedCards_6;
	// System.Int32 CardStackView::lastCount
	int32_t ___lastCount_7;
	// System.Single CardStackView::cardOffset
	float ___cardOffset_8;
};
