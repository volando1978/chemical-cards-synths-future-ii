﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardStackView
struct CardStackView_t4019401725;
// System.Object
struct Object_t;
// CardEventArgs
struct CardEventArgs_t2322276679;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_CardEventArgs2322276679.h"

// System.Void CardStackView::.ctor()
extern "C"  void CardStackView__ctor_m3668269582 (CardStackView_t4019401725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::Awake()
extern "C"  void CardStackView_Awake_m3905874801 (CardStackView_t4019401725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 CardStackView::WorldToGuiPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3525329789  CardStackView_WorldToGuiPoint_m3467429521 (CardStackView_t4019401725 * __this, Vector3_t3525329789  ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::Toggle(System.Int32,System.Boolean)
extern "C"  void CardStackView_Toggle_m4093617474 (CardStackView_t4019401725 * __this, int32_t ___card, bool ___isFaceUp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::Flipper(System.Int32)
extern "C"  void CardStackView_Flipper_m1991742797 (CardStackView_t4019401725 * __this, int32_t ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::Clear()
extern "C"  void CardStackView_Clear_m1074402873 (CardStackView_t4019401725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::deck_CardRemoved(System.Object,CardEventArgs)
extern "C"  void CardStackView_deck_CardRemoved_m2977399625 (CardStackView_t4019401725 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::deck_CardAdded(System.Object,CardEventArgs)
extern "C"  void CardStackView_deck_CardAdded_m1550079721 (CardStackView_t4019401725 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::Update()
extern "C"  void CardStackView_Update_m3774069439 (CardStackView_t4019401725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::ShowCards()
extern "C"  void CardStackView_ShowCards_m1217471122 (CardStackView_t4019401725 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStackView::AddCard(UnityEngine.Vector3,System.Int32,System.Int32)
extern "C"  void CardStackView_AddCard_m1972338588 (CardStackView_t4019401725 * __this, Vector3_t3525329789  ___position, int32_t ___cardIndex, int32_t ___positionalIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
