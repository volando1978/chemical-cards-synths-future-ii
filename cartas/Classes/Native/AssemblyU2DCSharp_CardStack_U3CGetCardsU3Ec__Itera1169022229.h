﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardStack
struct CardStack_t1747837240;

#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748.h"

// CardStack/<GetCards>c__Iterator1
struct  U3CGetCardsU3Ec__Iterator1_t1169022229  : public Object_t
{
	// System.Collections.Generic.List`1/Enumerator<System.Int32> CardStack/<GetCards>c__Iterator1::<$s_1>__0
	Enumerator_t1730156748  ___U3CU24s_1U3E__0_0;
	// System.Int32 CardStack/<GetCards>c__Iterator1::<i>__1
	int32_t ___U3CiU3E__1_1;
	// System.Int32 CardStack/<GetCards>c__Iterator1::$PC
	int32_t ___U24PC_2;
	// System.Int32 CardStack/<GetCards>c__Iterator1::$current
	int32_t ___U24current_3;
	// CardStack CardStack/<GetCards>c__Iterator1::<>f__this
	CardStack_t1747837240 * ___U3CU3Ef__this_4;
};
