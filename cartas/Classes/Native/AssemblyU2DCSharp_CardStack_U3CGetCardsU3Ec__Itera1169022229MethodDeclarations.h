﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardStack/<GetCards>c__Iterator1
struct U3CGetCardsU3Ec__Iterator1_t1169022229;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;

#include "codegen/il2cpp-codegen.h"

// System.Void CardStack/<GetCards>c__Iterator1::.ctor()
extern "C"  void U3CGetCardsU3Ec__Iterator1__ctor_m291355727 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CardStack/<GetCards>c__Iterator1::System.Collections.Generic.IEnumerator<int>.get_Current()
extern "C"  int32_t U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CintU3E_get_Current_m3238865550 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CardStack/<GetCards>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Object_t * U3CGetCardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1092185729 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CardStack/<GetCards>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Object_t * U3CGetCardsU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m3669158338 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Int32> CardStack/<GetCards>c__Iterator1::System.Collections.Generic.IEnumerable<int>.GetEnumerator()
extern "C"  Object_t* U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CintU3E_GetEnumerator_m2817282919 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardStack/<GetCards>c__Iterator1::MoveNext()
extern "C"  bool U3CGetCardsU3Ec__Iterator1_MoveNext_m2887455533 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack/<GetCards>c__Iterator1::Dispose()
extern "C"  void U3CGetCardsU3Ec__Iterator1_Dispose_m718262476 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardStack/<GetCards>c__Iterator1::Reset()
extern "C"  void U3CGetCardsU3Ec__Iterator1_Reset_m2232755964 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
