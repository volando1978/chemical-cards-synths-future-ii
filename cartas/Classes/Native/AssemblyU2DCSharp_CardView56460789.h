﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "mscorlib_System_Object837106420.h"

// CardView
struct  CardView_t56460789  : public Object_t
{
	// UnityEngine.GameObject CardView::<Card>k__BackingField
	GameObject_t4012695102 * ___U3CCardU3Ek__BackingField_0;
	// System.Boolean CardView::<IsFaceUp>k__BackingField
	bool ___U3CIsFaceUpU3Ek__BackingField_1;
};
