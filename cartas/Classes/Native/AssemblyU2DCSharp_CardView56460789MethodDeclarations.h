﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CardView
struct CardView_t56460789;
// UnityEngine.GameObject
struct GameObject_t4012695102;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"

// System.Void CardView::.ctor(UnityEngine.GameObject)
extern "C"  void CardView__ctor_m1118532414 (CardView_t56460789 * __this, GameObject_t4012695102 * ___card, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject CardView::get_Card()
extern "C"  GameObject_t4012695102 * CardView_get_Card_m882630668 (CardView_t56460789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardView::set_Card(UnityEngine.GameObject)
extern "C"  void CardView_set_Card_m3481277571 (CardView_t56460789 * __this, GameObject_t4012695102 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CardView::get_IsFaceUp()
extern "C"  bool CardView_get_IsFaceUp_m3530738077 (CardView_t56460789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardView::set_IsFaceUp(System.Boolean)
extern "C"  void CardView_set_IsFaceUp_m3617913108 (CardView_t56460789 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CardView::FlipCard(UnityEngine.GameObject,System.Int32)
extern "C"  void CardView_FlipCard_m33499876 (CardView_t56460789 * __this, GameObject_t4012695102 * ___card, int32_t ___cardIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
