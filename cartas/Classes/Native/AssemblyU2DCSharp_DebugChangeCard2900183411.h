﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t4012695102;
// Card
struct Card_t2092848;
// CardFlipper
struct CardFlipper_t1465479232;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// DebugChangeCard
struct  DebugChangeCard_t2900183411  : public MonoBehaviour_t3012272455
{
	// UnityEngine.GameObject DebugChangeCard::card
	GameObject_t4012695102 * ___card_2;
	// Card DebugChangeCard::cardModel
	Card_t2092848 * ___cardModel_3;
	// CardFlipper DebugChangeCard::flipper
	CardFlipper_t1465479232 * ___flipper_4;
	// System.Int32 DebugChangeCard::cardIndex
	int32_t ___cardIndex_5;
};
