﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugChangeCard
struct DebugChangeCard_t2900183411;

#include "codegen/il2cpp-codegen.h"

// System.Void DebugChangeCard::.ctor()
extern "C"  void DebugChangeCard__ctor_m4230639448 (DebugChangeCard_t2900183411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugChangeCard::Awake()
extern "C"  void DebugChangeCard_Awake_m173277371 (DebugChangeCard_t2900183411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugChangeCard::OnGUI()
extern "C"  void DebugChangeCard_OnGUI_m3726038098 (DebugChangeCard_t2900183411 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
