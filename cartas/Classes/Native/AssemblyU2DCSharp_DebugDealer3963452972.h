﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardStack
struct CardStack_t1747837240;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// DebugDealer
struct  DebugDealer_t3963452972  : public MonoBehaviour_t3012272455
{
	// CardStack DebugDealer::player
	CardStack_t1747837240 * ___player_2;
	// CardStack DebugDealer::dealer
	CardStack_t1747837240 * ___dealer_3;
};
