﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DebugDealer
struct DebugDealer_t3963452972;

#include "codegen/il2cpp-codegen.h"

// System.Void DebugDealer::.ctor()
extern "C"  void DebugDealer__ctor_m4258999999 (DebugDealer_t3963452972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DebugDealer::OnGUI()
extern "C"  void DebugDealer_OnGUI_m3754398649 (DebugDealer_t3963452972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
