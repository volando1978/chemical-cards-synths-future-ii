﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Draggable
struct Draggable_t1699487085;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void Draggable::.ctor()
extern "C"  void Draggable__ctor_m2724512414 (Draggable_t1699487085 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Draggable::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Draggable_OnBeginDrag_m1528043620 (Draggable_t1699487085 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Draggable::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Draggable_OnDrag_m362468613 (Draggable_t1699487085 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Draggable::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Draggable_OnEndDrag_m415526642 (Draggable_t1699487085 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
