﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardStack
struct CardStack_t1747837240;
// UnityEngine.UI.Button
struct Button_t990034267;
// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// GameController
struct  GameController_t2782302542  : public MonoBehaviour_t3012272455
{
	// System.Int32 GameController::dealersFirstCard
	int32_t ___dealersFirstCard_2;
	// CardStack GameController::dealer
	CardStack_t1747837240 * ___dealer_3;
	// CardStack GameController::deck
	CardStack_t1747837240 * ___deck_4;
	// CardStack GameController::player
	CardStack_t1747837240 * ___player_5;
	// UnityEngine.UI.Button GameController::hitButton
	Button_t990034267 * ___hitButton_6;
	// UnityEngine.UI.Button GameController::stickButton
	Button_t990034267 * ___stickButton_7;
	// UnityEngine.UI.Button GameController::playAgainButton
	Button_t990034267 * ___playAgainButton_8;
	// UnityEngine.UI.Text GameController::winnerText
	Text_t3286458198 * ___winnerText_9;
};
