﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object837106420.h"

// GameController/<DealersTurn>c__Iterator2
struct  U3CDealersTurnU3Ec__Iterator2_t120626468  : public Object_t
{
	// System.Int32 GameController/<DealersTurn>c__Iterator2::$PC
	int32_t ___U24PC_0;
	// System.Object GameController/<DealersTurn>c__Iterator2::$current
	Object_t * ___U24current_1;
};
