﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameController/<DealersTurn>c__Iterator2
struct U3CDealersTurnU3Ec__Iterator2_t120626468;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameController/<DealersTurn>c__Iterator2::.ctor()
extern "C"  void U3CDealersTurnU3Ec__Iterator2__ctor_m1749842520 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<DealersTurn>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Object_t * U3CDealersTurnU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3523125636 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameController/<DealersTurn>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Object_t * U3CDealersTurnU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1460415768 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameController/<DealersTurn>c__Iterator2::MoveNext()
extern "C"  bool U3CDealersTurnU3Ec__Iterator2_MoveNext_m205371524 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<DealersTurn>c__Iterator2::Dispose()
extern "C"  void U3CDealersTurnU3Ec__Iterator2_Dispose_m2164732053 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameController/<DealersTurn>c__Iterator2::Reset()
extern "C"  void U3CDealersTurnU3Ec__Iterator2_Reset_m3691242757 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
