﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t3286458198;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// LifeHud
struct  LifeHud_t1834144827  : public MonoBehaviour_t3012272455
{
	// UnityEngine.UI.Text LifeHud::text
	Text_t3286458198 * ___text_2;
};
