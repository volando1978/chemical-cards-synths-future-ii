﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LifeHud
struct LifeHud_t1834144827;
// System.Object
struct Object_t;
// UpdateLifeHudEventArgs
struct UpdateLifeHudEventArgs_t2181688197;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_UpdateLifeHudEventArgs2181688197.h"

// System.Void LifeHud::.ctor()
extern "C"  void LifeHud__ctor_m1068625296 (LifeHud_t1834144827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LifeHud::Awake()
extern "C"  void LifeHud_Awake_m1306230515 (LifeHud_t1834144827 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LifeHud::myGameController_UpdateLife(System.Object,UpdateLifeHudEventArgs)
extern "C"  void LifeHud_myGameController_UpdateLife_m4259040581 (LifeHud_t1834144827 * __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
