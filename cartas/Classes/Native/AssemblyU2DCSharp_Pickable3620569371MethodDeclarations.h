﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pickable
struct Pickable_t3620569371;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"

// System.Void Pickable::.ctor()
extern "C"  void Pickable__ctor_m1829981152 (Pickable_t3620569371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pickable::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void Pickable_OnPointerClick_m3874987344 (Pickable_t3620569371 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
