﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"
#include "AssemblyU2DCSharp_ScreenHelper_ScreenEdge1575643241.h"

// ScreenHelper
struct  ScreenHelper_t2451640154  : public MonoBehaviour_t3012272455
{
	// ScreenHelper/ScreenEdge ScreenHelper::screenEdge
	int32_t ___screenEdge_2;
	// System.Single ScreenHelper::yOffset
	float ___yOffset_3;
	// System.Single ScreenHelper::xOffset
	float ___xOffset_4;
};
