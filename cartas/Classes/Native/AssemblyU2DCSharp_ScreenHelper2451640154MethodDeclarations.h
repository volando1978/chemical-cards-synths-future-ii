﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenHelper
struct ScreenHelper_t2451640154;

#include "codegen/il2cpp-codegen.h"

// System.Void ScreenHelper::.ctor()
extern "C"  void ScreenHelper__ctor_m4198074497 (ScreenHelper_t2451640154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
