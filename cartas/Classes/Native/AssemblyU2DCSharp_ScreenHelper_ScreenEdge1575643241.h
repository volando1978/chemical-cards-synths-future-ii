﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum2778772662.h"
#include "AssemblyU2DCSharp_ScreenHelper_ScreenEdge1575643241.h"

// ScreenHelper/ScreenEdge
struct  ScreenEdge_t1575643241 
{
	// System.Int32 ScreenHelper/ScreenEdge::value__
	int32_t ___value___1;
};
