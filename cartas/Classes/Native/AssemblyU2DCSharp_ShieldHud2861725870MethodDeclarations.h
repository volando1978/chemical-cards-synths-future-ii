﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShieldHud
struct ShieldHud_t2861725870;
// System.Object
struct Object_t;
// UpdateShieldHudEventArgs
struct UpdateShieldHudEventArgs_t2548007474;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "AssemblyU2DCSharp_UpdateShieldHudEventArgs2548007474.h"

// System.Void ShieldHud::.ctor()
extern "C"  void ShieldHud__ctor_m230326397 (ShieldHud_t2861725870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldHud::Awake()
extern "C"  void ShieldHud_Awake_m467931616 (ShieldHud_t2861725870 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShieldHud::myGameController_UpdateShield(System.Object,UpdateShieldHudEventArgs)
extern "C"  void ShieldHud_myGameController_UpdateShield_m104902354 (ShieldHud_t2861725870 * __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
