﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_EventArgs516466188.h"

// UpdateLifeHudEventArgs
struct  UpdateLifeHudEventArgs_t2181688197  : public EventArgs_t516466188
{
	// System.Int32 UpdateLifeHudEventArgs::<Life>k__BackingField
	int32_t ___U3CLifeU3Ek__BackingField_1;
};
