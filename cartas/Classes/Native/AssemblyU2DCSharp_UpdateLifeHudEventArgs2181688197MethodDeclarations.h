﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateLifeHudEventArgs
struct UpdateLifeHudEventArgs_t2181688197;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateLifeHudEventArgs::.ctor(System.Int32)
extern "C"  void UpdateLifeHudEventArgs__ctor_m361874183 (UpdateLifeHudEventArgs_t2181688197 * __this, int32_t ___life, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UpdateLifeHudEventArgs::get_Life()
extern "C"  int32_t UpdateLifeHudEventArgs_get_Life_m1055854817 (UpdateLifeHudEventArgs_t2181688197 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateLifeHudEventArgs::set_Life(System.Int32)
extern "C"  void UpdateLifeHudEventArgs_set_Life_m239895192 (UpdateLifeHudEventArgs_t2181688197 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
