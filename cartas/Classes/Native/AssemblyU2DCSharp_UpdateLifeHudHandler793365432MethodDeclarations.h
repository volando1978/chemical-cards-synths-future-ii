﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateLifeHudHandler
struct UpdateLifeHudHandler_t793365432;
// System.Object
struct Object_t;
// UpdateLifeHudEventArgs
struct UpdateLifeHudEventArgs_t2181688197;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_UpdateLifeHudEventArgs2181688197.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UpdateLifeHudHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateLifeHudHandler__ctor_m3112192095 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateLifeHudHandler::Invoke(System.Object,UpdateLifeHudEventArgs)
extern "C"  void UpdateLifeHudHandler_Invoke_m411995846 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateLifeHudHandler_t793365432(Il2CppObject* delegate, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e);
// System.IAsyncResult UpdateLifeHudHandler::BeginInvoke(System.Object,UpdateLifeHudEventArgs,System.AsyncCallback,System.Object)
extern "C"  Object_t * UpdateLifeHudHandler_BeginInvoke_m3918480583 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, AsyncCallback_t1363551830 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateLifeHudHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateLifeHudHandler_EndInvoke_m550289903 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
