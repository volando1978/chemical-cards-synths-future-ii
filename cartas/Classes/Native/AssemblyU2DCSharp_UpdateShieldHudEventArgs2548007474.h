﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_EventArgs516466188.h"

// UpdateShieldHudEventArgs
struct  UpdateShieldHudEventArgs_t2548007474  : public EventArgs_t516466188
{
	// System.Int32 UpdateShieldHudEventArgs::<Shield>k__BackingField
	int32_t ___U3CShieldU3Ek__BackingField_1;
};
