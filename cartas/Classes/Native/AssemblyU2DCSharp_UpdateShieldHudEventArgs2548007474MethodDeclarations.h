﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateShieldHudEventArgs
struct UpdateShieldHudEventArgs_t2548007474;

#include "codegen/il2cpp-codegen.h"

// System.Void UpdateShieldHudEventArgs::.ctor(System.Int32)
extern "C"  void UpdateShieldHudEventArgs__ctor_m1570989178 (UpdateShieldHudEventArgs_t2548007474 * __this, int32_t ___shield, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UpdateShieldHudEventArgs::get_Shield()
extern "C"  int32_t UpdateShieldHudEventArgs_get_Shield_m1869541243 (UpdateShieldHudEventArgs_t2548007474 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateShieldHudEventArgs::set_Shield(System.Int32)
extern "C"  void UpdateShieldHudEventArgs_set_Shield_m2822943794 (UpdateShieldHudEventArgs_t2548007474 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
