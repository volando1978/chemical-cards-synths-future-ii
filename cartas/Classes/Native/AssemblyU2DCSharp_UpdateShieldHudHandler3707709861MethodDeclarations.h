﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UpdateShieldHudHandler
struct UpdateShieldHudHandler_t3707709861;
// System.Object
struct Object_t;
// UpdateShieldHudEventArgs
struct UpdateShieldHudEventArgs_t2548007474;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "AssemblyU2DCSharp_UpdateShieldHudEventArgs2548007474.h"
#include "mscorlib_System_AsyncCallback1363551830.h"

// System.Void UpdateShieldHudHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateShieldHudHandler__ctor_m3346913740 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateShieldHudHandler::Invoke(System.Object,UpdateShieldHudEventArgs)
extern "C"  void UpdateShieldHudHandler_Invoke_m2445626150 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void pinvoke_delegate_wrapper_UpdateShieldHudHandler_t3707709861(Il2CppObject* delegate, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e);
// System.IAsyncResult UpdateShieldHudHandler::BeginInvoke(System.Object,UpdateShieldHudEventArgs,System.AsyncCallback,System.Object)
extern "C"  Object_t * UpdateShieldHudHandler_BeginInvoke_m1000213153 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, AsyncCallback_t1363551830 * ___callback, Object_t * ___object, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UpdateShieldHudHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateShieldHudHandler_EndInvoke_m1224610268 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___result, const MethodInfo* method) IL2CPP_METHOD_ATTR;
