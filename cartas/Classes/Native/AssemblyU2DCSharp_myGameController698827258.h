﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardStack
struct CardStack_t1747837240;
// myGameController
struct myGameController_t698827258;
// UpdateShieldHudHandler
struct UpdateShieldHudHandler_t3707709861;
// UpdateLifeHudHandler
struct UpdateLifeHudHandler_t793365432;

#include "UnityEngine_UnityEngine_MonoBehaviour3012272455.h"

// myGameController
struct  myGameController_t698827258  : public MonoBehaviour_t3012272455
{
	// CardStack myGameController::deck
	CardStack_t1747837240 * ___deck_2;
	// CardStack myGameController::dealer
	CardStack_t1747837240 * ___dealer_3;
	// CardStack myGameController::player
	CardStack_t1747837240 * ___player_4;
	// System.Int32 myGameController::life
	int32_t ___life_5;
	// System.Int32 myGameController::shield
	int32_t ___shield_6;
	// UpdateShieldHudHandler myGameController::UpdateShieldHud
	UpdateShieldHudHandler_t3707709861 * ___UpdateShieldHud_8;
	// UpdateLifeHudHandler myGameController::UpdateLifeHud
	UpdateLifeHudHandler_t793365432 * ___UpdateLifeHud_9;
	// System.Int32 myGameController::<Life>k__BackingField
	int32_t ___U3CLifeU3Ek__BackingField_10;
	// System.Int32 myGameController::<Shield>k__BackingField
	int32_t ___U3CShieldU3Ek__BackingField_11;
};
struct myGameController_t698827258_StaticFields{
	// myGameController myGameController::instance
	myGameController_t698827258 * ___instance_7;
};
