﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// myGameController
struct myGameController_t698827258;
// UpdateShieldHudHandler
struct UpdateShieldHudHandler_t3707709861;
// UpdateLifeHudHandler
struct UpdateLifeHudHandler_t793365432;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UpdateShieldHudHandler3707709861.h"
#include "AssemblyU2DCSharp_UpdateLifeHudHandler793365432.h"

// System.Void myGameController::.ctor()
extern "C"  void myGameController__ctor_m3758063073 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::.cctor()
extern "C"  void myGameController__cctor_m53742060 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::add_UpdateShieldHud(UpdateShieldHudHandler)
extern "C"  void myGameController_add_UpdateShieldHud_m3606511201 (myGameController_t698827258 * __this, UpdateShieldHudHandler_t3707709861 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::remove_UpdateShieldHud(UpdateShieldHudHandler)
extern "C"  void myGameController_remove_UpdateShieldHud_m2064041032 (myGameController_t698827258 * __this, UpdateShieldHudHandler_t3707709861 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::add_UpdateLifeHud(UpdateLifeHudHandler)
extern "C"  void myGameController_add_UpdateLifeHud_m2507554683 (myGameController_t698827258 * __this, UpdateLifeHudHandler_t793365432 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::remove_UpdateLifeHud(UpdateLifeHudHandler)
extern "C"  void myGameController_remove_UpdateLifeHud_m1823150306 (myGameController_t698827258 * __this, UpdateLifeHudHandler_t793365432 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 myGameController::get_Life()
extern "C"  int32_t myGameController_get_Life_m3530338390 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::set_Life(System.Int32)
extern "C"  void myGameController_set_Life_m1379915789 (myGameController_t698827258 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 myGameController::get_Shield()
extern "C"  int32_t myGameController_get_Shield_m2441234243 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::set_Shield(System.Int32)
extern "C"  void myGameController_set_Shield_m565064186 (myGameController_t698827258 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::Awake()
extern "C"  void myGameController_Awake_m3995668292 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::Start()
extern "C"  void myGameController_Start_m2705200865 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::StartGame()
extern "C"  void myGameController_StartGame_m1288972915 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::DealCards()
extern "C"  void myGameController_DealCards_m3776010390 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator myGameController::DealerGivesCards()
extern "C"  Object_t * myGameController_DealerGivesCards_m3368865797 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::HitCount(System.Int32,System.Int32)
extern "C"  void myGameController_HitCount_m689921927 (myGameController_t698827258 * __this, int32_t ___card, int32_t ___cardIndex, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void myGameController::RestartGame()
extern "C"  void myGameController_RestartGame_m2232649440 (myGameController_t698827258 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
