﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardStackView
struct CardStackView_t4019401725;
// System.Object
struct Object_t;
// myGameController
struct myGameController_t698827258;

#include "mscorlib_System_Object837106420.h"

// myGameController/<DealerGivesCards>c__Iterator3
struct  U3CDealerGivesCardsU3Ec__Iterator3_t78322666  : public Object_t
{
	// System.Int32 myGameController/<DealerGivesCards>c__Iterator3::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Int32 myGameController/<DealerGivesCards>c__Iterator3::<card>__1
	int32_t ___U3CcardU3E__1_1;
	// CardStackView myGameController/<DealerGivesCards>c__Iterator3::<view>__2
	CardStackView_t4019401725 * ___U3CviewU3E__2_2;
	// System.Int32 myGameController/<DealerGivesCards>c__Iterator3::$PC
	int32_t ___U24PC_3;
	// System.Object myGameController/<DealerGivesCards>c__Iterator3::$current
	Object_t * ___U24current_4;
	// myGameController myGameController/<DealerGivesCards>c__Iterator3::<>f__this
	myGameController_t698827258 * ___U3CU3Ef__this_5;
};
