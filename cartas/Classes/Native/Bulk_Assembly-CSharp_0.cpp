﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// Bucket
struct Bucket_t2000631306;
// Card
struct Card_t2092848;
// UnityEngine.UI.Image
struct Image_t3354615620;
// System.Object
struct Object_t;
// CardEventArgs
struct CardEventArgs_t2322276679;
// CardEventHandler
struct CardEventHandler_t1048327840;
// System.IAsyncResult
struct IAsyncResult_t537683269;
// System.AsyncCallback
struct AsyncCallback_t1363551830;
// CardFlipper
struct CardFlipper_t1465479232;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t2223784725;
// UnityEngine.Sprite
struct Sprite_t4006040370;
// System.Collections.IEnumerator
struct IEnumerator_t287207039;
// CardFlipper/<Flip>c__Iterator0
struct U3CFlipU3Ec__Iterator0_t2620986452;
// CardStack
struct CardStack_t1747837240;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t1424601847;
// CardStack/<GetCards>c__Iterator1
struct U3CGetCardsU3Ec__Iterator1_t1169022229;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t35553939;
// CardStackView
struct CardStackView_t4019401725;
// UnityEngine.GameObject
struct GameObject_t4012695102;
// UnityEngine.RectTransform
struct RectTransform_t3317474837;
// CardView
struct CardView_t56460789;
// DebugChangeCard
struct DebugChangeCard_t2900183411;
// DebugDealer
struct DebugDealer_t3963452972;
// Draggable
struct Draggable_t1699487085;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t3205101634;
// GameController
struct GameController_t2782302542;
// GameController/<DealersTurn>c__Iterator2
struct U3CDealersTurnU3Ec__Iterator2_t120626468;
// LifeHud
struct LifeHud_t1834144827;
// UnityEngine.UI.Text
struct Text_t3286458198;
// UpdateLifeHudEventArgs
struct UpdateLifeHudEventArgs_t2181688197;
// myGameController
struct myGameController_t698827258;
// UpdateShieldHudHandler
struct UpdateShieldHudHandler_t3707709861;
// UpdateLifeHudHandler
struct UpdateLifeHudHandler_t793365432;
// myGameController/<DealerGivesCards>c__Iterator3
struct U3CDealerGivesCardsU3Ec__Iterator3_t78322666;
// Pickable
struct Pickable_t3620569371;
// ScreenHelper
struct ScreenHelper_t2451640154;
// ShieldHud
struct ShieldHud_t2861725870;
// UpdateShieldHudEventArgs
struct UpdateShieldHudEventArgs_t2548007474;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array2840145358.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_Bucket2000631306.h"
#include "AssemblyU2DCSharp_Bucket2000631306MethodDeclarations.h"
#include "mscorlib_System_Void2779279689.h"
#include "UnityEngine_UnityEngine_MonoBehaviour3012272455MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3644373756.h"
#include "mscorlib_System_Int322847414787.h"
#include "AssemblyU2DCSharp_Card2092848.h"
#include "AssemblyU2DCSharp_Card2092848MethodDeclarations.h"
#include "mscorlib_System_Type2779229935MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3878351788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component2126946602MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform284553113MethodDeclarations.h"
#include "mscorlib_System_String968488902MethodDeclarations.h"
#include "mscorlib_System_Type2779229935.h"
#include "mscorlib_System_RuntimeTypeHandle1864875887.h"
#include "UnityEngine_UnityEngine_Object3878351788.h"
#include "UnityEngine_UnityEngine_Canvas3534013893.h"
#include "UnityEngine_UnityEngine_Transform284553113.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620.h"
#include "UnityEngine_UnityEngine_Component2126946602.h"
#include "mscorlib_System_String968488902.h"
#include "mscorlib_System_Object837106420.h"
#include "mscorlib_System_Boolean211005341.h"
#include "UnityEngine_UI_UnityEngine_UI_Image3354615620MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Sprite4006040370.h"
#include "AssemblyU2DCSharp_CardEventArgs2322276679.h"
#include "AssemblyU2DCSharp_CardEventArgs2322276679MethodDeclarations.h"
#include "mscorlib_System_EventArgs516466188MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardEventHandler1048327840.h"
#include "AssemblyU2DCSharp_CardEventHandler1048327840MethodDeclarations.h"
#include "mscorlib_System_IntPtr676692020.h"
#include "mscorlib_System_AsyncCallback1363551830.h"
#include "AssemblyU2DCSharp_CardFlipper1465479232.h"
#include "AssemblyU2DCSharp_CardFlipper1465479232MethodDeclarations.h"
#include "mscorlib_System_Single958209021.h"
#include "UnityEngine_UnityEngine_SpriteRenderer2223784725.h"
#include "UnityEngine_UnityEngine_Coroutine2246592261.h"
#include "AssemblyU2DCSharp_CardFlipper_U3CFlipU3Ec__Iterato2620986452MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardFlipper_U3CFlipU3Ec__Iterato2620986452.h"
#include "mscorlib_System_Object837106420MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate896427542MethodDeclarations.h"
#include "mscorlib_System_UInt32985925326.h"
#include "UnityEngine_UnityEngine_WaitForFixedUpdate896427542.h"
#include "mscorlib_System_NotSupportedException1374155497MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1374155497.h"
#include "AssemblyU2DCSharp_CardStack1747837240.h"
#include "AssemblyU2DCSharp_CardStack1747837240MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010MethodDeclarations.h"
#include "mscorlib_System_Delegate3660574010.h"
#include "AssemblyU2DCSharp_CardStack_U3CGetCardsU3Ec__Itera1169022229MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardStack_U3CGetCardsU3Ec__Itera1169022229.h"
#include "UnityEngine_UnityEngine_Random3963434288MethodDeclarations.h"
#include "mscorlib_System_Threading_Interlocked3007803305MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1730156748.h"
#include "AssemblyU2DCSharp_CardStackView4019401725.h"
#include "AssemblyU2DCSharp_CardStackView4019401725MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge547271572MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3994030297MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge547271572.h"
#include "UnityEngine_UnityEngine_Vector33525329789.h"
#include "UnityEngine_UnityEngine_Camera3533968274MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera3533968274.h"
#include "AssemblyU2DCSharp_CardView56460789MethodDeclarations.h"
#include "AssemblyU2DCSharp_CardView56460789.h"
#include "UnityEngine_UnityEngine_GameObject4012695102.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2469408666MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va314299513MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va314299513.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2469408666.h"
#include "UnityEngine_UnityEngine_Vector33525329789MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_GameObject4012695102MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer1092684080MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform3317474837.h"
#include "AssemblyU2DCSharp_DebugChangeCard2900183411.h"
#include "AssemblyU2DCSharp_DebugChangeCard2900183411MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI1522956648MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect1525428817.h"
#include "AssemblyU2DCSharp_DebugDealer3963452972.h"
#include "AssemblyU2DCSharp_DebugDealer3963452972MethodDeclarations.h"
#include "AssemblyU2DCSharp_Draggable1699487085.h"
#include "AssemblyU2DCSharp_Draggable1699487085MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve3205101634MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector23525329788.h"
#include "AssemblyU2DCSharp_GameController2782302542.h"
#include "AssemblyU2DCSharp_GameController2782302542MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable3621744255MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button990034267.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198.h"
#include "UnityEngine_UI_UnityEngine_UI_Text3286458198MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController_U3CDealersTurnU3Ec120626468MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController_U3CDealersTurnU3Ec120626468.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1291133240.h"
#include "AssemblyU2DCSharp_LifeHud1834144827.h"
#include "AssemblyU2DCSharp_LifeHud1834144827MethodDeclarations.h"
#include "AssemblyU2DCSharp_UpdateLifeHudHandler793365432MethodDeclarations.h"
#include "AssemblyU2DCSharp_myGameController698827258MethodDeclarations.h"
#include "AssemblyU2DCSharp_myGameController698827258.h"
#include "AssemblyU2DCSharp_UpdateLifeHudEventArgs2181688197.h"
#include "AssemblyU2DCSharp_UpdateLifeHudHandler793365432.h"
#include "AssemblyU2DCSharp_UpdateLifeHudEventArgs2181688197MethodDeclarations.h"
#include "AssemblyU2DCSharp_UpdateShieldHudHandler3707709861.h"
#include "AssemblyU2DCSharp_UpdateShieldHudEventArgs2548007474MethodDeclarations.h"
#include "AssemblyU2DCSharp_UpdateShieldHudHandler3707709861MethodDeclarations.h"
#include "AssemblyU2DCSharp_UpdateShieldHudEventArgs2548007474.h"
#include "AssemblyU2DCSharp_myGameController_U3CDealerGivesCar78322666MethodDeclarations.h"
#include "AssemblyU2DCSharp_myGameController_U3CDealerGivesCar78322666.h"
#include "AssemblyU2DCSharp_Pickable3620569371.h"
#include "AssemblyU2DCSharp_Pickable3620569371MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScreenHelper2451640154.h"
#include "AssemblyU2DCSharp_ScreenHelper2451640154MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScreenHelper_ScreenEdge1575643241.h"
#include "AssemblyU2DCSharp_ScreenHelper_ScreenEdge1575643241MethodDeclarations.h"
#include "AssemblyU2DCSharp_ShieldHud2861725870.h"
#include "AssemblyU2DCSharp_ShieldHud2861725870MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Object_t * Component_GetComponent_TisObject_t_m267839954_gshared (Component_t2126946602 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m267839954(__this, method) ((  Object_t * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t3354615620_m3706520426(__this, method) ((  Image_t3354615620 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, method) ((  SpriteRenderer_t2223784725 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<Card>()
#define Component_GetComponent_TisCard_t2092848_m829011029(__this, method) ((  Card_t2092848 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CardStack>()
#define Component_GetComponent_TisCardStack_t1747837240_m2694384001(__this, method) ((  CardStack_t1747837240 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Object_t * GameObject_GetComponent_TisObject_t_m2447772384_gshared (GameObject_t4012695102 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m2447772384(__this, method) ((  Object_t * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Card>()
#define GameObject_GetComponent_TisCard_t2092848_m3913226925(__this, method) ((  Card_t2092848 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Object_t * Object_Instantiate_TisObject_t_m3133387403_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m3133387403(__this /* static, unused */, p0, method) ((  Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t4012695102_m3917608929(__this /* static, unused */, p0, method) ((  GameObject_t4012695102 * (*) (Object_t * /* static, unused */, GameObject_t4012695102 *, const MethodInfo*))Object_Instantiate_TisObject_t_m3133387403_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
#define GameObject_GetComponent_TisRectTransform_t3317474837_m406276429(__this, method) ((  RectTransform_t3317474837 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.SpriteRenderer>()
#define GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113(__this, method) ((  SpriteRenderer_t2223784725 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CardFlipper>()
#define GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153(__this, method) ((  CardFlipper_t1465479232 * (*) (GameObject_t4012695102 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CardStackView>()
#define Component_GetComponent_TisCardStackView_t4019401725_m2584442780(__this, method) ((  CardStackView_t4019401725 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t3286458198_m1610753993(__this, method) ((  Text_t3286458198 * (*) (Component_t2126946602 *, const MethodInfo*))Component_GetComponent_TisObject_t_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Bucket::.ctor()
extern "C"  void Bucket__ctor_m2493733329 (Bucket_t2000631306 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bucket::Start()
extern "C"  void Bucket_Start_m1440871121 (Bucket_t2000631306 * __this, const MethodInfo* method)
{
	{
		Bucket_CreateBucket_m3162219481(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Bucket::CreateBucket()
extern TypeInfo* List_1_t3644373756_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1634217978_MethodInfo_var;
extern const uint32_t Bucket_CreateBucket_m3162219481_MetadataUsageId;
extern "C"  void Bucket_CreateBucket_m3162219481 (Bucket_t2000631306 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Bucket_CreateBucket_m3162219481_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3644373756 * L_0 = (List_1_t3644373756 *)il2cpp_codegen_object_new(List_1_t3644373756_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_0, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		__this->___listaQ_4 = L_0;
		List_1_t3644373756 * L_1 = (List_1_t3644373756 *)il2cpp_codegen_object_new(List_1_t3644373756_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_1, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		__this->___listaS_5 = L_1;
		List_1_t3644373756 * L_2 = (List_1_t3644373756 *)il2cpp_codegen_object_new(List_1_t3644373756_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_2, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		__this->___listaF_6 = L_2;
		return;
	}
}
// System.Void Bucket::PushQ(System.Int32)
extern "C"  void Bucket_PushQ_m652156407 (Bucket_t2000631306 * __this, int32_t ___card, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = (__this->___listaQ_4);
		int32_t L_1 = ___card;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void Bucket::PushS(System.Int32)
extern "C"  void Bucket_PushS_m3667260025 (Bucket_t2000631306 * __this, int32_t ___card, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = (__this->___listaS_5);
		int32_t L_1 = ___card;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void Bucket::PushF(System.Int32)
extern "C"  void Bucket_PushF_m1248955692 (Bucket_t2000631306 * __this, int32_t ___card, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = (__this->___listaF_6);
		int32_t L_1 = ___card;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_0, L_1);
		return;
	}
}
// System.Void Card::.ctor()
extern "C"  void Card__ctor_m245477739 (Card_t2092848 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Card::get_CardIndex()
extern "C"  int32_t Card_get_CardIndex_m542160116 (Card_t2092848 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CCardIndexU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void Card::set_CardIndex(System.Int32)
extern "C"  void Card_set_CardIndex_m3891891615 (Card_t2092848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CCardIndexU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Int32 Card::get_CardValue()
extern "C"  int32_t Card_get_CardValue_m3125440979 (Card_t2092848 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CCardValueU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void Card::set_CardValue(System.Int32)
extern "C"  void Card_set_CardValue_m1364980734 (Card_t2092848 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CCardValueU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.Void Card::Awake()
extern const Il2CppType* Canvas_t3534013893_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* Canvas_t3534013893_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t3354615620_m3706520426_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral298874201;
extern const uint32_t Card_Awake_m483082958_MetadataUsageId;
extern "C"  void Card_Awake_m483082958 (Card_t2092848 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Card_Awake_m483082958_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Canvas_t3534013893_0_0_0_var), /*hidden argument*/NULL);
		Object_t3878351788 * L_1 = Object_FindObjectOfType_m3820159265(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		__this->___canvas_7 = ((Canvas_t3534013893 *)CastclassSealed(L_1, Canvas_t3534013893_il2cpp_TypeInfo_var));
		Transform_t284553113 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Canvas_t3534013893 * L_3 = (__this->___canvas_7);
		NullCheck(L_3);
		Transform_t284553113 * L_4 = Component_get_transform_m4257140443(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_SetParent_m3449663462(L_2, L_4, /*hidden argument*/NULL);
		Image_t3354615620 * L_5 = Component_GetComponent_TisImage_t3354615620_m3706520426(__this, /*hidden argument*/Component_GetComponent_TisImage_t3354615620_m3706520426_MethodInfo_var);
		__this->___image_2 = L_5;
		int32_t L_6 = (__this->___cardIndex_5);
		int32_t L_7 = L_6;
		Object_t * L_8 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_7);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral298874201, L_8, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Card::AssignValue(System.Int32)
extern Il2CppCodeGenString* _stringLiteral4236355879;
extern Il2CppCodeGenString* _stringLiteral2581695604;
extern const uint32_t Card_AssignValue_m3124969166_MetadataUsageId;
extern "C"  int32_t Card_AssignValue_m3124969166 (Card_t2092848 * __this, int32_t ___cardIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Card_AssignValue_m3124969166_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___cardIndex;
		__this->___cardValue_6 = ((int32_t)((int32_t)L_0%(int32_t)((int32_t)13)));
		int32_t L_1 = (__this->___cardValue_6);
		__this->___cardValue_6 = ((int32_t)((int32_t)L_1+(int32_t)1));
		int32_t L_2 = (__this->___cardValue_6);
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral4236355879, /*hidden argument*/NULL);
	}

IL_002e:
	{
		int32_t L_3 = (__this->___cardValue_6);
		if ((((int32_t)L_3) >= ((int32_t)1)))
		{
			goto IL_0044;
		}
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral2581695604, /*hidden argument*/NULL);
	}

IL_0044:
	{
		int32_t L_4 = (__this->___cardValue_6);
		return L_4;
	}
}
// System.Void Card::ToggleCard(System.Boolean)
extern "C"  void Card_ToggleCard_m309718708 (Card_t2092848 * __this, bool ___showFace, const MethodInfo* method)
{
	{
		bool L_0 = ___showFace;
		if (!L_0)
		{
			goto IL_0023;
		}
	}
	{
		Image_t3354615620 * L_1 = (__this->___image_2);
		SpriteU5BU5D_t503173063* L_2 = (__this->___faces_3);
		int32_t L_3 = (__this->___cardIndex_5);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck(L_1);
		Image_set_sprite_m572551402(L_1, (*(Sprite_t4006040370 **)(Sprite_t4006040370 **)SZArrayLdElema(L_2, L_4, sizeof(Sprite_t4006040370 *))), /*hidden argument*/NULL);
		goto IL_0034;
	}

IL_0023:
	{
		Image_t3354615620 * L_5 = (__this->___image_2);
		Sprite_t4006040370 * L_6 = (__this->___cardBack_4);
		NullCheck(L_5);
		Image_set_sprite_m572551402(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0034:
	{
		return;
	}
}
// System.Void CardEventArgs::.ctor(System.Int32)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t CardEventArgs__ctor_m1327271765_MetadataUsageId;
extern "C"  void CardEventArgs__ctor_m1327271765 (CardEventArgs_t2322276679 * __this, int32_t ___cardIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardEventArgs__ctor_m1327271765_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___cardIndex;
		CardEventArgs_set_CardIndex_m3495056824(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 CardEventArgs::get_CardIndex()
extern "C"  int32_t CardEventArgs_get_CardIndex_m3530186089 (CardEventArgs_t2322276679 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CCardIndexU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void CardEventArgs::set_CardIndex(System.Int32)
extern "C"  void CardEventArgs_set_CardIndex_m3495056824 (CardEventArgs_t2322276679 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CCardIndexU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Void CardEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void CardEventHandler__ctor_m1251021639 (CardEventHandler_t1048327840 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void CardEventHandler::Invoke(System.Object,CardEventArgs)
extern "C"  void CardEventHandler_Invoke_m524776984 (CardEventHandler_t1048327840 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		CardEventHandler_Invoke_m524776984((CardEventHandler_t1048327840 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, void* __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,il2cpp_codegen_get_delegate_this(__this),___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(il2cpp_codegen_get_delegate_this(__this),___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, CardEventArgs_t2322276679 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_CardEventHandler_t1048327840(Il2CppObject* delegate, Object_t * ___sender, CardEventArgs_t2322276679 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult CardEventHandler::BeginInvoke(System.Object,CardEventArgs,System.AsyncCallback,System.Object)
extern "C"  Object_t * CardEventHandler_BeginInvoke_m1222706639 (CardEventHandler_t1048327840 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, AsyncCallback_t1363551830 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void CardEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void CardEventHandler_EndInvoke_m579289303 (CardEventHandler_t1048327840 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void CardFlipper::.ctor()
extern "C"  void CardFlipper__ctor_m1674335019 (CardFlipper_t1465479232 * __this, const MethodInfo* method)
{
	{
		__this->___duration_5 = (0.5f);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardFlipper::Awake()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCard_t2092848_m829011029_MethodInfo_var;
extern const uint32_t CardFlipper_Awake_m1911940238_MetadataUsageId;
extern "C"  void CardFlipper_Awake_m1911940238 (CardFlipper_t1465479232 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardFlipper_Awake_m1911940238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRenderer_t2223784725 * L_0 = Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t2223784725_m4090179846_MethodInfo_var);
		__this->___spriterenderer_2 = L_0;
		Card_t2092848 * L_1 = Component_GetComponent_TisCard_t2092848_m829011029(__this, /*hidden argument*/Component_GetComponent_TisCard_t2092848_m829011029_MethodInfo_var);
		__this->___cardModel_3 = L_1;
		return;
	}
}
// System.Void CardFlipper::FlipCard(UnityEngine.Sprite,UnityEngine.Sprite,System.Int32)
extern "C"  void CardFlipper_FlipCard_m3019275047 (CardFlipper_t1465479232 * __this, Sprite_t4006040370 * ___startImage, Sprite_t4006040370 * ___endImage, int32_t ___cardIndex, const MethodInfo* method)
{
	{
		Sprite_t4006040370 * L_0 = ___startImage;
		Sprite_t4006040370 * L_1 = ___endImage;
		int32_t L_2 = ___cardIndex;
		Object_t * L_3 = CardFlipper_Flip_m3737881903(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		MonoBehaviour_StopCoroutine_m1340700766(__this, L_3, /*hidden argument*/NULL);
		Sprite_t4006040370 * L_4 = ___startImage;
		Sprite_t4006040370 * L_5 = ___endImage;
		int32_t L_6 = ___cardIndex;
		Object_t * L_7 = CardFlipper_Flip_m3737881903(__this, L_4, L_5, L_6, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator CardFlipper::Flip(UnityEngine.Sprite,UnityEngine.Sprite,System.Int32)
extern TypeInfo* U3CFlipU3Ec__Iterator0_t2620986452_il2cpp_TypeInfo_var;
extern const uint32_t CardFlipper_Flip_m3737881903_MetadataUsageId;
extern "C"  Object_t * CardFlipper_Flip_m3737881903 (CardFlipper_t1465479232 * __this, Sprite_t4006040370 * ___startImage, Sprite_t4006040370 * ___endImage, int32_t ___cardIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardFlipper_Flip_m3737881903_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFlipU3Ec__Iterator0_t2620986452 * V_0 = {0};
	{
		U3CFlipU3Ec__Iterator0_t2620986452 * L_0 = (U3CFlipU3Ec__Iterator0_t2620986452 *)il2cpp_codegen_object_new(U3CFlipU3Ec__Iterator0_t2620986452_il2cpp_TypeInfo_var);
		U3CFlipU3Ec__Iterator0__ctor_m503058808(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFlipU3Ec__Iterator0_t2620986452 * L_1 = V_0;
		return L_1;
	}
}
// System.Void CardFlipper/<Flip>c__Iterator0::.ctor()
extern "C"  void U3CFlipU3Ec__Iterator0__ctor_m503058808 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CardFlipper/<Flip>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Object_t * U3CFlipU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2884500644 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object CardFlipper/<Flip>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Object_t * U3CFlipU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m4099262520 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean CardFlipper/<Flip>c__Iterator0::MoveNext()
extern TypeInfo* WaitForFixedUpdate_t896427542_il2cpp_TypeInfo_var;
extern const uint32_t U3CFlipU3Ec__Iterator0_MoveNext_m3402620516_MetadataUsageId;
extern "C"  bool U3CFlipU3Ec__Iterator0_MoveNext_m3402620516 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFlipU3Ec__Iterator0_MoveNext_m3402620516_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0038;
		}
	}
	{
		goto IL_003f;
	}

IL_0021:
	{
		WaitForFixedUpdate_t896427542 * L_2 = (WaitForFixedUpdate_t896427542 *)il2cpp_codegen_object_new(WaitForFixedUpdate_t896427542_il2cpp_TypeInfo_var);
		WaitForFixedUpdate__ctor_m2916734308(L_2, /*hidden argument*/NULL);
		__this->___U24current_1 = L_2;
		__this->___U24PC_0 = 1;
		goto IL_0041;
	}

IL_0038:
	{
		__this->___U24PC_0 = (-1);
	}

IL_003f:
	{
		return (bool)0;
	}

IL_0041:
	{
		return (bool)1;
	}
	// Dead block : IL_0043: ldloc.1
}
// System.Void CardFlipper/<Flip>c__Iterator0::Dispose()
extern "C"  void U3CFlipU3Ec__Iterator0_Dispose_m2301460405 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void CardFlipper/<Flip>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CFlipU3Ec__Iterator0_Reset_m2444459045_MetadataUsageId;
extern "C"  void U3CFlipU3Ec__Iterator0_Reset_m2444459045 (U3CFlipU3Ec__Iterator0_t2620986452 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFlipU3Ec__Iterator0_Reset_m2444459045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CardStack::.ctor()
extern "C"  void CardStack__ctor_m4094366515 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardStack::add_CardRemoved(CardEventHandler)
extern TypeInfo* CardEventHandler_t1048327840_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_add_CardRemoved_m4115931587_MetadataUsageId;
extern "C"  void CardStack_add_CardRemoved_m4115931587 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_add_CardRemoved_m4115931587_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardEventHandler_t1048327840 * L_0 = (__this->___CardRemoved_4);
		CardEventHandler_t1048327840 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___CardRemoved_4 = ((CardEventHandler_t1048327840 *)CastclassSealed(L_2, CardEventHandler_t1048327840_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void CardStack::remove_CardRemoved(CardEventHandler)
extern TypeInfo* CardEventHandler_t1048327840_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_remove_CardRemoved_m4014251398_MetadataUsageId;
extern "C"  void CardStack_remove_CardRemoved_m4014251398 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_remove_CardRemoved_m4014251398_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardEventHandler_t1048327840 * L_0 = (__this->___CardRemoved_4);
		CardEventHandler_t1048327840 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___CardRemoved_4 = ((CardEventHandler_t1048327840 *)CastclassSealed(L_2, CardEventHandler_t1048327840_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void CardStack::add_CardAdded(CardEventHandler)
extern TypeInfo* CardEventHandler_t1048327840_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_add_CardAdded_m219470563_MetadataUsageId;
extern "C"  void CardStack_add_CardAdded_m219470563 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_add_CardAdded_m219470563_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardEventHandler_t1048327840 * L_0 = (__this->___CardAdded_5);
		CardEventHandler_t1048327840 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___CardAdded_5 = ((CardEventHandler_t1048327840 *)CastclassSealed(L_2, CardEventHandler_t1048327840_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void CardStack::remove_CardAdded(CardEventHandler)
extern TypeInfo* CardEventHandler_t1048327840_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_remove_CardAdded_m3513215846_MetadataUsageId;
extern "C"  void CardStack_remove_CardAdded_m3513215846 (CardStack_t1747837240 * __this, CardEventHandler_t1048327840 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_remove_CardAdded_m3513215846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardEventHandler_t1048327840 * L_0 = (__this->___CardAdded_5);
		CardEventHandler_t1048327840 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___CardAdded_5 = ((CardEventHandler_t1048327840 *)CastclassSealed(L_2, CardEventHandler_t1048327840_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void CardStack::Awake()
extern TypeInfo* List_1_t3644373756_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1634217978_MethodInfo_var;
extern const uint32_t CardStack_Awake_m37004438_MetadataUsageId;
extern "C"  void CardStack_Awake_m37004438 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_Awake_m37004438_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3644373756 * L_0 = (List_1_t3644373756 *)il2cpp_codegen_object_new(List_1_t3644373756_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_0, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		__this->___cards_2 = L_0;
		bool L_1 = (__this->___isGameDeck_3);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		CardStack_CreateDeck_m726432310(__this, /*hidden argument*/NULL);
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 CardStack::get_CardCount()
extern "C"  int32_t CardStack_get_CardCount_m2692224405 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = (__this->___cards_2);
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		List_1_t3644373756 * L_1 = (__this->___cards_2);
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Int32> CardStack::GetCards()
extern TypeInfo* U3CGetCardsU3Ec__Iterator1_t1169022229_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_GetCards_m3171586759_MetadataUsageId;
extern "C"  Object_t* CardStack_GetCards_m3171586759 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_GetCards_m3171586759_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetCardsU3Ec__Iterator1_t1169022229 * V_0 = {0};
	{
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_0 = (U3CGetCardsU3Ec__Iterator1_t1169022229 *)il2cpp_codegen_object_new(U3CGetCardsU3Ec__Iterator1_t1169022229_il2cpp_TypeInfo_var);
		U3CGetCardsU3Ec__Iterator1__ctor_m291355727(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_2 = V_0;
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_3 = L_2;
		NullCheck(L_3);
		L_3->___U24PC_2 = ((int32_t)-2);
		return L_3;
	}
}
// System.Boolean CardStack::get_HasCards()
extern "C"  bool CardStack_get_HasCards_m3672825519 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		List_1_t3644373756 * L_0 = (__this->___cards_2);
		if (!L_0)
		{
			goto IL_001b;
		}
	}
	{
		List_1_t3644373756 * L_1 = (__this->___cards_2);
		NullCheck(L_1);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_1);
		G_B3_0 = ((((int32_t)L_2) > ((int32_t)0))? 1 : 0);
		goto IL_001c;
	}

IL_001b:
	{
		G_B3_0 = 0;
	}

IL_001c:
	{
		return (bool)G_B3_0;
	}
}
// System.Int32 CardStack::Pop()
extern TypeInfo* CardEventArgs_t2322276679_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_Pop_m3559568496_MetadataUsageId;
extern "C"  int32_t CardStack_Pop_m3559568496 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_Pop_m3559568496_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		List_1_t3644373756 * L_0 = (__this->___cards_2);
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_0, 0);
		V_0 = L_1;
		List_1_t3644373756 * L_2 = (__this->___cards_2);
		NullCheck(L_2);
		VirtActionInvoker1< int32_t >::Invoke(30 /* System.Void System.Collections.Generic.List`1<System.Int32>::RemoveAt(System.Int32) */, L_2, 0);
		CardEventHandler_t1048327840 * L_3 = (__this->___CardRemoved_4);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		CardEventHandler_t1048327840 * L_4 = (__this->___CardRemoved_4);
		int32_t L_5 = V_0;
		CardEventArgs_t2322276679 * L_6 = (CardEventArgs_t2322276679 *)il2cpp_codegen_object_new(CardEventArgs_t2322276679_il2cpp_TypeInfo_var);
		CardEventArgs__ctor_m1327271765(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		CardEventHandler_Invoke_m524776984(L_4, __this, L_6, /*hidden argument*/NULL);
	}

IL_0036:
	{
		int32_t L_7 = V_0;
		return L_7;
	}
}
// System.Void CardStack::RemoveCard(System.Int32)
extern TypeInfo* CardEventArgs_t2322276679_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_RemoveCard_m3746222646_MetadataUsageId;
extern "C"  void CardStack_RemoveCard_m3746222646 (CardStack_t1747837240 * __this, int32_t ___card, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_RemoveCard_m3746222646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardEventHandler_t1048327840 * L_0 = (__this->___CardRemoved_4);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		CardEventHandler_t1048327840 * L_1 = (__this->___CardRemoved_4);
		int32_t L_2 = ___card;
		CardEventArgs_t2322276679 * L_3 = (CardEventArgs_t2322276679 *)il2cpp_codegen_object_new(CardEventArgs_t2322276679_il2cpp_TypeInfo_var);
		CardEventArgs__ctor_m1327271765(L_3, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		CardEventHandler_Invoke_m524776984(L_1, __this, L_3, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void CardStack::Push(System.Int32)
extern TypeInfo* CardEventArgs_t2322276679_il2cpp_TypeInfo_var;
extern const uint32_t CardStack_Push_m1325389084_MetadataUsageId;
extern "C"  void CardStack_Push_m1325389084 (CardStack_t1747837240 * __this, int32_t ___card, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStack_Push_m1325389084_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t3644373756 * L_0 = (__this->___cards_2);
		int32_t L_1 = ___card;
		NullCheck(L_0);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_0, L_1);
		CardEventHandler_t1048327840 * L_2 = (__this->___CardAdded_5);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		CardEventHandler_t1048327840 * L_3 = (__this->___CardAdded_5);
		int32_t L_4 = ___card;
		CardEventArgs_t2322276679 * L_5 = (CardEventArgs_t2322276679 *)il2cpp_codegen_object_new(CardEventArgs_t2322276679_il2cpp_TypeInfo_var);
		CardEventArgs__ctor_m1327271765(L_5, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		CardEventHandler_Invoke_m524776984(L_3, __this, L_5, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Void CardStack::CreateDeck()
extern "C"  void CardStack_CreateDeck_m726432310 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		List_1_t3644373756 * L_0 = (__this->___cards_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Int32>::Clear() */, L_0);
		V_0 = 0;
		goto IL_0022;
	}

IL_0012:
	{
		List_1_t3644373756 * L_1 = (__this->___cards_2);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		VirtActionInvoker1< int32_t >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Int32>::Add(!0) */, L_1, L_2);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0022:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)((int32_t)52))))
		{
			goto IL_0012;
		}
	}
	{
		List_1_t3644373756 * L_5 = (__this->___cards_2);
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<System.Int32>::get_Count() */, L_5);
		V_1 = L_6;
		goto IL_007b;
	}

IL_003b:
	{
		int32_t L_7 = V_1;
		V_1 = ((int32_t)((int32_t)L_7-(int32_t)1));
		int32_t L_8 = V_1;
		int32_t L_9 = Random_Range_m75452833(NULL /*static, unused*/, 0, ((int32_t)((int32_t)L_8+(int32_t)1)), /*hidden argument*/NULL);
		V_2 = L_9;
		List_1_t3644373756 * L_10 = (__this->___cards_2);
		int32_t L_11 = V_2;
		NullCheck(L_10);
		int32_t L_12 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_10, L_11);
		V_3 = L_12;
		List_1_t3644373756 * L_13 = (__this->___cards_2);
		int32_t L_14 = V_2;
		List_1_t3644373756 * L_15 = (__this->___cards_2);
		int32_t L_16 = V_1;
		NullCheck(L_15);
		int32_t L_17 = VirtFuncInvoker1< int32_t, int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32) */, L_15, L_16);
		NullCheck(L_13);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0) */, L_13, L_14, L_17);
		List_1_t3644373756 * L_18 = (__this->___cards_2);
		int32_t L_19 = V_1;
		int32_t L_20 = V_3;
		NullCheck(L_18);
		VirtActionInvoker2< int32_t, int32_t >::Invoke(32 /* System.Void System.Collections.Generic.List`1<System.Int32>::set_Item(System.Int32,!0) */, L_18, L_19, L_20);
	}

IL_007b:
	{
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) > ((int32_t)1)))
		{
			goto IL_003b;
		}
	}
	{
		return;
	}
}
// System.Void CardStack::Reset()
extern "C"  void CardStack_Reset_m1740799456 (CardStack_t1747837240 * __this, const MethodInfo* method)
{
	{
		List_1_t3644373756 * L_0 = (__this->___cards_2);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<System.Int32>::Clear() */, L_0);
		return;
	}
}
// System.Void CardStack/<GetCards>c__Iterator1::.ctor()
extern "C"  void U3CGetCardsU3Ec__Iterator1__ctor_m291355727 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 CardStack/<GetCards>c__Iterator1::System.Collections.Generic.IEnumerator<int>.get_Current()
extern "C"  int32_t U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CintU3E_get_Current_m3238865550 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object CardStack/<GetCards>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetCardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1092185729_MetadataUsageId;
extern "C"  Object_t * U3CGetCardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1092185729 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCardsU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m1092185729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___U24current_3);
		int32_t L_1 = L_0;
		Object_t * L_2 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_1);
		return L_2;
	}
}
// System.Collections.IEnumerator CardStack/<GetCards>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Object_t * U3CGetCardsU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m3669158338 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	{
		Object_t* L_0 = U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CintU3E_GetEnumerator_m2817282919(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Int32> CardStack/<GetCards>c__Iterator1::System.Collections.Generic.IEnumerable<int>.GetEnumerator()
extern TypeInfo* U3CGetCardsU3Ec__Iterator1_t1169022229_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CintU3E_GetEnumerator_m2817282919_MetadataUsageId;
extern "C"  Object_t* U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CintU3E_GetEnumerator_m2817282919 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCardsU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CintU3E_GetEnumerator_m2817282919_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetCardsU3Ec__Iterator1_t1169022229 * V_0 = {0};
	{
		int32_t* L_0 = &(__this->___U24PC_2);
		int32_t L_1 = Interlocked_CompareExchange_m1859820752(NULL /*static, unused*/, L_0, 0, ((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_2 = (U3CGetCardsU3Ec__Iterator1_t1169022229 *)il2cpp_codegen_object_new(U3CGetCardsU3Ec__Iterator1_t1169022229_il2cpp_TypeInfo_var);
		U3CGetCardsU3Ec__Iterator1__ctor_m291355727(L_2, /*hidden argument*/NULL);
		V_0 = L_2;
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_3 = V_0;
		CardStack_t1747837240 * L_4 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_3);
		L_3->___U3CU3Ef__this_4 = L_4;
		U3CGetCardsU3Ec__Iterator1_t1169022229 * L_5 = V_0;
		return L_5;
	}
}
// System.Boolean CardStack/<GetCards>c__Iterator1::MoveNext()
extern TypeInfo* Enumerator_t1730156748_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m883489159_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m1836990579_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m3459783625_MethodInfo_var;
extern const uint32_t U3CGetCardsU3Ec__Iterator1_MoveNext_m2887455533_MetadataUsageId;
extern "C"  bool U3CGetCardsU3Ec__Iterator1_MoveNext_m2887455533 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCardsU3Ec__Iterator1_MoveNext_m2887455533_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0023;
		}
		if (L_1 == 1)
		{
			goto IL_003c;
		}
	}
	{
		goto IL_00a9;
	}

IL_0023:
	{
		CardStack_t1747837240 * L_2 = (__this->___U3CU3Ef__this_4);
		NullCheck(L_2);
		List_1_t3644373756 * L_3 = (L_2->___cards_2);
		NullCheck(L_3);
		Enumerator_t1730156748  L_4 = List_1_GetEnumerator_m883489159(L_3, /*hidden argument*/List_1_GetEnumerator_m883489159_MethodInfo_var);
		__this->___U3CU24s_1U3E__0_0 = L_4;
		V_0 = ((int32_t)-3);
	}

IL_003c:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_5 = V_0;
			if (((int32_t)((int32_t)L_5-(int32_t)1)) == 0)
			{
				goto IL_0078;
			}
		}

IL_0048:
		{
			goto IL_0078;
		}

IL_004d:
		{
			Enumerator_t1730156748 * L_6 = &(__this->___U3CU24s_1U3E__0_0);
			int32_t L_7 = Enumerator_get_Current_m1836990579(L_6, /*hidden argument*/Enumerator_get_Current_m1836990579_MethodInfo_var);
			__this->___U3CiU3E__1_1 = L_7;
			int32_t L_8 = (__this->___U3CiU3E__1_1);
			__this->___U24current_3 = L_8;
			__this->___U24PC_2 = 1;
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xAB, FINALLY_008d);
		}

IL_0078:
		{
			Enumerator_t1730156748 * L_9 = &(__this->___U3CU24s_1U3E__0_0);
			bool L_10 = Enumerator_MoveNext_m3459783625(L_9, /*hidden argument*/Enumerator_MoveNext_m3459783625_MethodInfo_var);
			if (L_10)
			{
				goto IL_004d;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0xA2, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			bool L_11 = V_1;
			if (!L_11)
			{
				goto IL_0091;
			}
		}

IL_0090:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0091:
		{
			Enumerator_t1730156748  L_12 = (__this->___U3CU24s_1U3E__0_0);
			Enumerator_t1730156748  L_13 = L_12;
			Object_t * L_14 = Box(Enumerator_t1730156748_il2cpp_TypeInfo_var, &L_13);
			NullCheck((Object_t *)L_14);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Object_t *)L_14);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
		IL2CPP_JUMP_TBL(0xA2, IL_00a2)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_00a2:
	{
		__this->___U24PC_2 = (-1);
	}

IL_00a9:
	{
		return (bool)0;
	}

IL_00ab:
	{
		return (bool)1;
	}
	// Dead block : IL_00ad: ldloc.2
}
// System.Void CardStack/<GetCards>c__Iterator1::Dispose()
extern TypeInfo* Enumerator_t1730156748_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetCardsU3Ec__Iterator1_Dispose_m718262476_MetadataUsageId;
extern "C"  void U3CGetCardsU3Ec__Iterator1_Dispose_m718262476 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCardsU3Ec__Iterator1_Dispose_m718262476_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0037;
		}
		if (L_1 == 1)
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0037;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x37, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		Enumerator_t1730156748  L_2 = (__this->___U3CU24s_1U3E__0_0);
		Enumerator_t1730156748  L_3 = L_2;
		Object_t * L_4 = Box(Enumerator_t1730156748_il2cpp_TypeInfo_var, &L_3);
		NullCheck((Object_t *)L_4);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Object_t *)L_4);
		IL2CPP_END_FINALLY(38)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x37, IL_0037)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0037:
	{
		return;
	}
}
// System.Void CardStack/<GetCards>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CGetCardsU3Ec__Iterator1_Reset_m2232755964_MetadataUsageId;
extern "C"  void U3CGetCardsU3Ec__Iterator1_Reset_m2232755964 (U3CGetCardsU3Ec__Iterator1_t1169022229 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CGetCardsU3Ec__Iterator1_Reset_m2232755964_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void CardStackView::.ctor()
extern "C"  void CardStackView__ctor_m3668269582 (CardStackView_t4019401725 * __this, const MethodInfo* method)
{
	{
		__this->___cardOffset_8 = (1.0f);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardStackView::Awake()
extern TypeInfo* Dictionary_2_t547271572_il2cpp_TypeInfo_var;
extern TypeInfo* CardEventHandler_t1048327840_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m179939658_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCardStack_t1747837240_m2694384001_MethodInfo_var;
extern const MethodInfo* CardStackView_deck_CardRemoved_m2977399625_MethodInfo_var;
extern const MethodInfo* CardStackView_deck_CardAdded_m1550079721_MethodInfo_var;
extern const uint32_t CardStackView_Awake_m3905874801_MetadataUsageId;
extern "C"  void CardStackView_Awake_m3905874801 (CardStackView_t4019401725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStackView_Awake_m3905874801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t547271572 * L_0 = (Dictionary_2_t547271572 *)il2cpp_codegen_object_new(Dictionary_2_t547271572_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m179939658(L_0, /*hidden argument*/Dictionary_2__ctor_m179939658_MethodInfo_var);
		__this->___fetchedCards_6 = L_0;
		CardStack_t1747837240 * L_1 = Component_GetComponent_TisCardStack_t1747837240_m2694384001(__this, /*hidden argument*/Component_GetComponent_TisCardStack_t1747837240_m2694384001_MethodInfo_var);
		__this->___deck_2 = L_1;
		CardStackView_ShowCards_m1217471122(__this, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_2 = (__this->___deck_2);
		NullCheck(L_2);
		int32_t L_3 = CardStack_get_CardCount_m2692224405(L_2, /*hidden argument*/NULL);
		__this->___lastCount_7 = L_3;
		CardStack_t1747837240 * L_4 = (__this->___deck_2);
		IntPtr_t L_5 = { (void*)CardStackView_deck_CardRemoved_m2977399625_MethodInfo_var };
		CardEventHandler_t1048327840 * L_6 = (CardEventHandler_t1048327840 *)il2cpp_codegen_object_new(CardEventHandler_t1048327840_il2cpp_TypeInfo_var);
		CardEventHandler__ctor_m1251021639(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		CardStack_add_CardRemoved_m4115931587(L_4, L_6, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_7 = (__this->___deck_2);
		IntPtr_t L_8 = { (void*)CardStackView_deck_CardAdded_m1550079721_MethodInfo_var };
		CardEventHandler_t1048327840 * L_9 = (CardEventHandler_t1048327840 *)il2cpp_codegen_object_new(CardEventHandler_t1048327840_il2cpp_TypeInfo_var);
		CardEventHandler__ctor_m1251021639(L_9, __this, L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		CardStack_add_CardAdded_m219470563(L_7, L_9, /*hidden argument*/NULL);
		int32_t L_10 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___cardOffset_8 = (((float)((float)((int32_t)((int32_t)L_10/(int32_t)4)))));
		return;
	}
}
// UnityEngine.Vector3 CardStackView::WorldToGuiPoint(UnityEngine.Vector3)
extern "C"  Vector3_t3525329789  CardStackView_WorldToGuiPoint_m3467429521 (CardStackView_t4019401725 * __this, Vector3_t3525329789  ___position, const MethodInfo* method)
{
	Vector3_t3525329789  V_0 = {0};
	{
		Camera_t3533968274 * L_0 = Camera_get_main_m671815697(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t3525329789  L_1 = ___position;
		NullCheck(L_0);
		Vector3_t3525329789  L_2 = Camera_WorldToScreenPoint_m2400233676(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((float)((float)(((float)((float)L_3)))-(float)L_4));
		Vector3_t3525329789  L_5 = V_0;
		return L_5;
	}
}
// System.Void CardStackView::Toggle(System.Int32,System.Boolean)
extern "C"  void CardStackView_Toggle_m4093617474 (CardStackView_t4019401725 * __this, int32_t ___card, bool ___isFaceUp, const MethodInfo* method)
{
	{
		Dictionary_2_t547271572 * L_0 = (__this->___fetchedCards_6);
		int32_t L_1 = ___card;
		NullCheck(L_0);
		CardView_t56460789 * L_2 = VirtFuncInvoker1< CardView_t56460789 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,CardView>::get_Item(!0) */, L_0, L_1);
		bool L_3 = ___isFaceUp;
		NullCheck(L_2);
		CardView_set_IsFaceUp_m3617913108(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardStackView::Flipper(System.Int32)
extern "C"  void CardStackView_Flipper_m1991742797 (CardStackView_t4019401725 * __this, int32_t ___card, const MethodInfo* method)
{
	{
		Dictionary_2_t547271572 * L_0 = (__this->___fetchedCards_6);
		int32_t L_1 = ___card;
		NullCheck(L_0);
		CardView_t56460789 * L_2 = VirtFuncInvoker1< CardView_t56460789 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,CardView>::get_Item(!0) */, L_0, L_1);
		Dictionary_2_t547271572 * L_3 = (__this->___fetchedCards_6);
		int32_t L_4 = ___card;
		NullCheck(L_3);
		CardView_t56460789 * L_5 = VirtFuncInvoker1< CardView_t56460789 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,CardView>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		GameObject_t4012695102 * L_6 = CardView_get_Card_m882630668(L_5, /*hidden argument*/NULL);
		int32_t L_7 = ___card;
		NullCheck(L_2);
		CardView_FlipCard_m33499876(L_2, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardStackView::Clear()
extern TypeInfo* Enumerator_t314299513_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_get_Values_m1640311148_MethodInfo_var;
extern const MethodInfo* ValueCollection_GetEnumerator_m1507377088_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m2226557018_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m1611801909_MethodInfo_var;
extern const uint32_t CardStackView_Clear_m1074402873_MetadataUsageId;
extern "C"  void CardStackView_Clear_m1074402873 (CardStackView_t4019401725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStackView_Clear_m1074402873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	CardView_t56460789 * V_0 = {0};
	Enumerator_t314299513  V_1 = {0};
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		CardStack_t1747837240 * L_0 = (__this->___deck_2);
		NullCheck(L_0);
		CardStack_Reset_m1740799456(L_0, /*hidden argument*/NULL);
		Dictionary_2_t547271572 * L_1 = (__this->___fetchedCards_6);
		NullCheck(L_1);
		ValueCollection_t2469408666 * L_2 = Dictionary_2_get_Values_m1640311148(L_1, /*hidden argument*/Dictionary_2_get_Values_m1640311148_MethodInfo_var);
		NullCheck(L_2);
		Enumerator_t314299513  L_3 = ValueCollection_GetEnumerator_m1507377088(L_2, /*hidden argument*/ValueCollection_GetEnumerator_m1507377088_MethodInfo_var);
		V_1 = L_3;
	}

IL_001c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0034;
		}

IL_0021:
		{
			CardView_t56460789 * L_4 = Enumerator_get_Current_m2226557018((&V_1), /*hidden argument*/Enumerator_get_Current_m2226557018_MethodInfo_var);
			V_0 = L_4;
			CardView_t56460789 * L_5 = V_0;
			NullCheck(L_5);
			GameObject_t4012695102 * L_6 = CardView_get_Card_m882630668(L_5, /*hidden argument*/NULL);
			Object_Destroy_m176400816(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		}

IL_0034:
		{
			bool L_7 = Enumerator_MoveNext_m1611801909((&V_1), /*hidden argument*/Enumerator_MoveNext_m1611801909_MethodInfo_var);
			if (L_7)
			{
				goto IL_0021;
			}
		}

IL_0040:
		{
			IL2CPP_LEAVE(0x51, FINALLY_0045);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0045;
	}

FINALLY_0045:
	{ // begin finally (depth: 1)
		Enumerator_t314299513  L_8 = V_1;
		Enumerator_t314299513  L_9 = L_8;
		Object_t * L_10 = Box(Enumerator_t314299513_il2cpp_TypeInfo_var, &L_9);
		NullCheck((Object_t *)L_10);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, (Object_t *)L_10);
		IL2CPP_END_FINALLY(69)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(69)
	{
		IL2CPP_JUMP_TBL(0x51, IL_0051)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_0051:
	{
		Dictionary_2_t547271572 * L_11 = (__this->___fetchedCards_6);
		NullCheck(L_11);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,CardView>::Clear() */, L_11);
		return;
	}
}
// System.Void CardStackView::deck_CardRemoved(System.Object,CardEventArgs)
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2313189240;
extern const uint32_t CardStackView_deck_CardRemoved_m2977399625_MetadataUsageId;
extern "C"  void CardStackView_deck_CardRemoved_m2977399625 (CardStackView_t4019401725 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStackView_deck_CardRemoved_m2977399625_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardEventArgs_t2322276679 * L_0 = ___e;
		NullCheck(L_0);
		int32_t L_1 = CardEventArgs_get_CardIndex_m3530186089(L_0, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2313189240, L_3, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Dictionary_2_t547271572 * L_5 = (__this->___fetchedCards_6);
		CardEventArgs_t2322276679 * L_6 = ___e;
		NullCheck(L_6);
		int32_t L_7 = CardEventArgs_get_CardIndex_m3530186089(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_8 = VirtFuncInvoker1< bool, int32_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,CardView>::ContainsKey(!0) */, L_5, L_7);
		if (!L_8)
		{
			goto IL_005d;
		}
	}
	{
		Dictionary_2_t547271572 * L_9 = (__this->___fetchedCards_6);
		CardEventArgs_t2322276679 * L_10 = ___e;
		NullCheck(L_10);
		int32_t L_11 = CardEventArgs_get_CardIndex_m3530186089(L_10, /*hidden argument*/NULL);
		NullCheck(L_9);
		CardView_t56460789 * L_12 = VirtFuncInvoker1< CardView_t56460789 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,CardView>::get_Item(!0) */, L_9, L_11);
		NullCheck(L_12);
		GameObject_t4012695102 * L_13 = CardView_get_Card_m882630668(L_12, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		Dictionary_2_t547271572 * L_14 = (__this->___fetchedCards_6);
		CardEventArgs_t2322276679 * L_15 = ___e;
		NullCheck(L_15);
		int32_t L_16 = CardEventArgs_get_CardIndex_m3530186089(L_15, /*hidden argument*/NULL);
		NullCheck(L_14);
		VirtFuncInvoker1< bool, int32_t >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,CardView>::Remove(!0) */, L_14, L_16);
	}

IL_005d:
	{
		return;
	}
}
// System.Void CardStackView::deck_CardAdded(System.Object,CardEventArgs)
extern TypeInfo* ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t3525329789_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3467305968;
extern Il2CppCodeGenString* _stringLiteral110245606;
extern Il2CppCodeGenString* _stringLiteral114980;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t CardStackView_deck_CardAdded_m1550079721_MetadataUsageId;
extern "C"  void CardStackView_deck_CardAdded_m1550079721 (CardStackView_t4019401725 * __this, Object_t * ___sender, CardEventArgs_t2322276679 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStackView_deck_CardAdded_m1550079721_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t3525329789  V_1 = {0};
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral3467305968, /*hidden argument*/NULL);
		float L_0 = (__this->___cardOffset_8);
		CardStack_t1747837240 * L_1 = (__this->___deck_2);
		NullCheck(L_1);
		int32_t L_2 = CardStack_get_CardCount_m2692224405(L_1, /*hidden argument*/NULL);
		V_0 = ((float)((float)L_0*(float)(((float)((float)L_2)))));
		Transform_t284553113 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t3525329789  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		float L_5 = V_0;
		Vector3_t3525329789  L_6 = {0};
		Vector3__ctor_m1846874791(&L_6, L_5, (0.0f), /*hidden argument*/NULL);
		Vector3_t3525329789  L_7 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		ObjectU5BU5D_t11523773* L_8 = ((ObjectU5BU5D_t11523773*)SZArrayNew(ObjectU5BU5D_t11523773_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		ArrayElementTypeCheck (L_8, _stringLiteral110245606);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral110245606;
		ObjectU5BU5D_t11523773* L_9 = L_8;
		Vector3_t3525329789  L_10 = V_1;
		Vector3_t3525329789  L_11 = L_10;
		Object_t * L_12 = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 1);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 1, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t11523773* L_13 = L_9;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 2);
		ArrayElementTypeCheck (L_13, _stringLiteral114980);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral114980;
		ObjectU5BU5D_t11523773* L_14 = L_13;
		Transform_t284553113 * L_15 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_15);
		Vector3_t3525329789  L_16 = Transform_get_position_m2211398607(L_15, /*hidden argument*/NULL);
		Vector3_t3525329789  L_17 = L_16;
		Object_t * L_18 = Box(Vector3_t3525329789_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 3);
		ArrayElementTypeCheck (L_14, L_18);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_14, 3, sizeof(Object_t *))) = (Object_t *)L_18;
		ObjectU5BU5D_t11523773* L_19 = L_14;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 4);
		ArrayElementTypeCheck (L_19, _stringLiteral32);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_19, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral32;
		ObjectU5BU5D_t11523773* L_20 = L_19;
		CardStack_t1747837240 * L_21 = (__this->___deck_2);
		NullCheck(L_21);
		int32_t L_22 = CardStack_get_CardCount_m2692224405(L_21, /*hidden argument*/NULL);
		int32_t L_23 = L_22;
		Object_t * L_24 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 5);
		ArrayElementTypeCheck (L_20, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_20, 5, sizeof(Object_t *))) = (Object_t *)L_24;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m3016520001(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		Vector3_t3525329789  L_26 = V_1;
		CardEventArgs_t2322276679 * L_27 = ___e;
		NullCheck(L_27);
		int32_t L_28 = CardEventArgs_get_CardIndex_m3530186089(L_27, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_29 = (__this->___deck_2);
		NullCheck(L_29);
		int32_t L_30 = CardStack_get_CardCount_m2692224405(L_29, /*hidden argument*/NULL);
		CardStackView_AddCard_m1972338588(__this, L_26, L_28, L_30, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CardStackView::Update()
extern "C"  void CardStackView_Update_m3774069439 (CardStackView_t4019401725 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___lastCount_7);
		CardStack_t1747837240 * L_1 = (__this->___deck_2);
		NullCheck(L_1);
		int32_t L_2 = CardStack_get_CardCount_m2692224405(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_0) == ((int32_t)L_2)))
		{
			goto IL_002d;
		}
	}
	{
		CardStack_t1747837240 * L_3 = (__this->___deck_2);
		NullCheck(L_3);
		int32_t L_4 = CardStack_get_CardCount_m2692224405(L_3, /*hidden argument*/NULL);
		__this->___lastCount_7 = L_4;
		CardStackView_ShowCards_m1217471122(__this, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void CardStackView::ShowCards()
extern TypeInfo* IEnumerable_1_t1424601847_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t35553939_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t287207039_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t1628921374_il2cpp_TypeInfo_var;
extern const uint32_t CardStackView_ShowCards_m1217471122_MetadataUsageId;
extern "C"  void CardStackView_ShowCards_m1217471122 (CardStackView_t4019401725 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStackView_ShowCards_m1217471122_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Object_t* V_2 = {0};
	float V_3 = 0.0f;
	Vector3_t3525329789  V_4 = {0};
	Exception_t1967233988 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1967233988 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		V_0 = 0;
		CardStack_t1747837240 * L_0 = (__this->___deck_2);
		NullCheck(L_0);
		bool L_1 = CardStack_get_HasCards_m3672825519(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_007e;
		}
	}
	{
		CardStack_t1747837240 * L_2 = (__this->___deck_2);
		NullCheck(L_2);
		Object_t* L_3 = CardStack_GetCards_m3171586759(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Object_t* L_4 = InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Int32>::GetEnumerator() */, IEnumerable_1_t1424601847_il2cpp_TypeInfo_var, L_3);
		V_2 = L_4;
	}

IL_0023:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0063;
		}

IL_0028:
		{
			Object_t* L_5 = V_2;
			NullCheck(L_5);
			int32_t L_6 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Int32>::get_Current() */, IEnumerator_1_t35553939_il2cpp_TypeInfo_var, L_5);
			V_1 = L_6;
			int32_t L_7 = V_0;
			V_3 = ((float)((float)(((float)((float)L_7)))*(float)(0.2f)));
			Transform_t284553113 * L_8 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
			NullCheck(L_8);
			Vector3_t3525329789  L_9 = Transform_get_position_m2211398607(L_8, /*hidden argument*/NULL);
			float L_10 = V_3;
			Vector3_t3525329789  L_11 = {0};
			Vector3__ctor_m1846874791(&L_11, (0.0f), L_10, /*hidden argument*/NULL);
			Vector3_t3525329789  L_12 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
			V_4 = L_12;
			Vector3_t3525329789  L_13 = V_4;
			int32_t L_14 = V_1;
			int32_t L_15 = V_0;
			CardStackView_AddCard_m1972338588(__this, L_13, L_14, L_15, /*hidden argument*/NULL);
			int32_t L_16 = V_0;
			V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
		}

IL_0063:
		{
			Object_t* L_17 = V_2;
			NullCheck(L_17);
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t287207039_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_0028;
			}
		}

IL_006e:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0073);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1967233988 *)e.ex;
		goto FINALLY_0073;
	}

FINALLY_0073:
	{ // begin finally (depth: 1)
		{
			Object_t* L_19 = V_2;
			if (L_19)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(115)
		}

IL_0077:
		{
			Object_t* L_20 = V_2;
			NullCheck(L_20);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1628921374_il2cpp_TypeInfo_var, L_20);
			IL2CPP_END_FINALLY(115)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(115)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1967233988 *)
	}

IL_007e:
	{
		return;
	}
}
// System.Void CardStackView::AddCard(UnityEngine.Vector3,System.Int32,System.Int32)
extern TypeInfo* CardView_t56460789_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var;
extern const MethodInfo* Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRectTransform_t3317474837_m406276429_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var;
extern const uint32_t CardStackView_AddCard_m1972338588_MetadataUsageId;
extern "C"  void CardStackView_AddCard_m1972338588 (CardStackView_t4019401725 * __this, Vector3_t3525329789  ___position, int32_t ___cardIndex, int32_t ___positionalIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardStackView_AddCard_m1972338588_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Card_t2092848 * V_0 = {0};
	GameObject_t4012695102 * V_1 = {0};
	Card_t2092848 * V_2 = {0};
	SpriteRenderer_t2223784725 * V_3 = {0};
	{
		Dictionary_2_t547271572 * L_0 = (__this->___fetchedCards_6);
		int32_t L_1 = ___cardIndex;
		NullCheck(L_0);
		bool L_2 = VirtFuncInvoker1< bool, int32_t >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,CardView>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		bool L_3 = (__this->___faceUp_3);
		if (L_3)
		{
			goto IL_004a;
		}
	}
	{
		Dictionary_2_t547271572 * L_4 = (__this->___fetchedCards_6);
		int32_t L_5 = ___cardIndex;
		NullCheck(L_4);
		CardView_t56460789 * L_6 = VirtFuncInvoker1< CardView_t56460789 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,CardView>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		GameObject_t4012695102 * L_7 = CardView_get_Card_m882630668(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Card_t2092848 * L_8 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_7, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		V_0 = L_8;
		Card_t2092848 * L_9 = V_0;
		Dictionary_2_t547271572 * L_10 = (__this->___fetchedCards_6);
		int32_t L_11 = ___cardIndex;
		NullCheck(L_10);
		CardView_t56460789 * L_12 = VirtFuncInvoker1< CardView_t56460789 *, int32_t >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.Int32,CardView>::get_Item(!0) */, L_10, L_11);
		NullCheck(L_12);
		bool L_13 = CardView_get_IsFaceUp_m3530738077(L_12, /*hidden argument*/NULL);
		NullCheck(L_9);
		Card_ToggleCard_m309718708(L_9, L_13, /*hidden argument*/NULL);
	}

IL_004a:
	{
		return;
	}

IL_004b:
	{
		GameObject_t4012695102 * L_14 = (__this->___cardPrefab_5);
		GameObject_t4012695102 * L_15 = Object_Instantiate_TisGameObject_t4012695102_m3917608929(NULL /*static, unused*/, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_t4012695102_m3917608929_MethodInfo_var);
		V_1 = L_15;
		GameObject_t4012695102 * L_16 = V_1;
		NullCheck(L_16);
		Transform_t284553113 * L_17 = GameObject_get_transform_m1278640159(L_16, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_18 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		Transform_t284553113 * L_19 = GameObject_get_transform_m1278640159(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		Transform_SetParent_m263985879(L_17, L_19, (bool)0, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_20 = V_1;
		NullCheck(L_20);
		RectTransform_t3317474837 * L_21 = GameObject_GetComponent_TisRectTransform_t3317474837_m406276429(L_20, /*hidden argument*/GameObject_GetComponent_TisRectTransform_t3317474837_m406276429_MethodInfo_var);
		Vector3_t3525329789  L_22 = ___position;
		NullCheck(L_21);
		Transform_set_position_m3111394108(L_21, L_22, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_23 = V_1;
		NullCheck(L_23);
		Transform_t284553113 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		Vector3_t3525329789  L_25 = Vector3_get_one_m886467710(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_24);
		Transform_set_localScale_m310756934(L_24, L_25, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_26 = V_1;
		NullCheck(L_26);
		Card_t2092848 * L_27 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_26, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		V_2 = L_27;
		Card_t2092848 * L_28 = V_2;
		int32_t L_29 = ___cardIndex;
		NullCheck(L_28);
		L_28->___cardIndex_5 = L_29;
		Card_t2092848 * L_30 = V_2;
		Card_t2092848 * L_31 = V_2;
		int32_t L_32 = ___cardIndex;
		NullCheck(L_31);
		int32_t L_33 = Card_AssignValue_m3124969166(L_31, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		Card_set_CardValue_m1364980734(L_30, L_33, /*hidden argument*/NULL);
		Card_t2092848 * L_34 = V_2;
		bool L_35 = (__this->___faceUp_3);
		NullCheck(L_34);
		Card_ToggleCard_m309718708(L_34, L_35, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_36 = V_1;
		NullCheck(L_36);
		SpriteRenderer_t2223784725 * L_37 = GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113(L_36, /*hidden argument*/GameObject_GetComponent_TisSpriteRenderer_t2223784725_m3588502113_MethodInfo_var);
		V_3 = L_37;
		bool L_38 = (__this->___reverseLayerOrder_4);
		if (!L_38)
		{
			goto IL_00d2;
		}
	}
	{
		SpriteRenderer_t2223784725 * L_39 = V_3;
		int32_t L_40 = ___positionalIndex;
		NullCheck(L_39);
		Renderer_set_sortingOrder_m3971126610(L_39, ((int32_t)((int32_t)((int32_t)51)-(int32_t)L_40)), /*hidden argument*/NULL);
		goto IL_00d9;
	}

IL_00d2:
	{
		SpriteRenderer_t2223784725 * L_41 = V_3;
		int32_t L_42 = ___positionalIndex;
		NullCheck(L_41);
		Renderer_set_sortingOrder_m3971126610(L_41, L_42, /*hidden argument*/NULL);
	}

IL_00d9:
	{
		Dictionary_2_t547271572 * L_43 = (__this->___fetchedCards_6);
		int32_t L_44 = ___cardIndex;
		GameObject_t4012695102 * L_45 = V_1;
		CardView_t56460789 * L_46 = (CardView_t56460789 *)il2cpp_codegen_object_new(CardView_t56460789_il2cpp_TypeInfo_var);
		CardView__ctor_m1118532414(L_46, L_45, /*hidden argument*/NULL);
		NullCheck(L_43);
		VirtActionInvoker2< int32_t, CardView_t56460789 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.Int32,CardView>::Add(!0,!1) */, L_43, L_44, L_46);
		return;
	}
}
// System.Void CardView::.ctor(UnityEngine.GameObject)
extern "C"  void CardView__ctor_m1118532414 (CardView_t56460789 * __this, GameObject_t4012695102 * ___card, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_0 = ___card;
		CardView_set_Card_m3481277571(__this, L_0, /*hidden argument*/NULL);
		CardView_set_IsFaceUp_m3617913108(__this, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject CardView::get_Card()
extern "C"  GameObject_t4012695102 * CardView_get_Card_m882630668 (CardView_t56460789 * __this, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = (__this->___U3CCardU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void CardView::set_Card(UnityEngine.GameObject)
extern "C"  void CardView_set_Card_m3481277571 (CardView_t56460789 * __this, GameObject_t4012695102 * ___value, const MethodInfo* method)
{
	{
		GameObject_t4012695102 * L_0 = ___value;
		__this->___U3CCardU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.Boolean CardView::get_IsFaceUp()
extern "C"  bool CardView_get_IsFaceUp_m3530738077 (CardView_t56460789 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CIsFaceUpU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void CardView::set_IsFaceUp(System.Boolean)
extern "C"  void CardView_set_IsFaceUp_m3617913108 (CardView_t56460789 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CIsFaceUpU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Void CardView::FlipCard(UnityEngine.GameObject,System.Int32)
extern const MethodInfo* GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153_MethodInfo_var;
extern const uint32_t CardView_FlipCard_m33499876_MetadataUsageId;
extern "C"  void CardView_FlipCard_m33499876 (CardView_t56460789 * __this, GameObject_t4012695102 * ___card, int32_t ___cardIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CardView_FlipCard_m33499876_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Card_t2092848 * V_0 = {0};
	{
		GameObject_t4012695102 * L_0 = ___card;
		NullCheck(L_0);
		Card_t2092848 * L_1 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_0, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		V_0 = L_1;
		GameObject_t4012695102 * L_2 = ___card;
		NullCheck(L_2);
		CardFlipper_t1465479232 * L_3 = GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153(L_2, /*hidden argument*/GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153_MethodInfo_var);
		Card_t2092848 * L_4 = V_0;
		NullCheck(L_4);
		Sprite_t4006040370 * L_5 = (L_4->___cardBack_4);
		Card_t2092848 * L_6 = V_0;
		NullCheck(L_6);
		SpriteU5BU5D_t503173063* L_7 = (L_6->___faces_3);
		int32_t L_8 = ___cardIndex;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = ___cardIndex;
		NullCheck(L_3);
		CardFlipper_FlipCard_m3019275047(L_3, L_5, (*(Sprite_t4006040370 **)(Sprite_t4006040370 **)SZArrayLdElema(L_7, L_9, sizeof(Sprite_t4006040370 *))), L_10, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugChangeCard::.ctor()
extern "C"  void DebugChangeCard__ctor_m4230639448 (DebugChangeCard_t2900183411 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugChangeCard::Awake()
extern const MethodInfo* GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153_MethodInfo_var;
extern const uint32_t DebugChangeCard_Awake_m173277371_MetadataUsageId;
extern "C"  void DebugChangeCard_Awake_m173277371 (DebugChangeCard_t2900183411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugChangeCard_Awake_m173277371_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t4012695102 * L_0 = (__this->___card_2);
		NullCheck(L_0);
		Card_t2092848 * L_1 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_0, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		__this->___cardModel_3 = L_1;
		GameObject_t4012695102 * L_2 = (__this->___card_2);
		NullCheck(L_2);
		CardFlipper_t1465479232 * L_3 = GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153(L_2, /*hidden argument*/GameObject_GetComponent_TisCardFlipper_t1465479232_m4194037153_MethodInfo_var);
		__this->___flipper_4 = L_3;
		return;
	}
}
// System.Void DebugChangeCard::OnGUI()
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2161758565;
extern const uint32_t DebugChangeCard_OnGUI_m3726038098_MetadataUsageId;
extern "C"  void DebugChangeCard_OnGUI_m3726038098 (DebugChangeCard_t2900183411 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugChangeCard_OnGUI_m3726038098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0 = {0};
		Rect__ctor_m3291325233(&L_0, (10.0f), (10.0f), (100.0f), (28.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m885093907(NULL /*static, unused*/, L_0, _stringLiteral2161758565, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0102;
		}
	}
	{
		int32_t L_2 = (__this->___cardIndex_5);
		Card_t2092848 * L_3 = (__this->___cardModel_3);
		NullCheck(L_3);
		SpriteU5BU5D_t503173063* L_4 = (L_3->___faces_3);
		NullCheck(L_4);
		if ((((int32_t)L_2) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_4)->max_length)))))))
		{
			goto IL_007e;
		}
	}
	{
		__this->___cardIndex_5 = 0;
		CardFlipper_t1465479232 * L_5 = (__this->___flipper_4);
		Card_t2092848 * L_6 = (__this->___cardModel_3);
		NullCheck(L_6);
		SpriteU5BU5D_t503173063* L_7 = (L_6->___faces_3);
		Card_t2092848 * L_8 = (__this->___cardModel_3);
		NullCheck(L_8);
		SpriteU5BU5D_t503173063* L_9 = (L_8->___faces_3);
		NullCheck(L_9);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))-(int32_t)1)));
		int32_t L_10 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Array_t *)L_9)->max_length))))-(int32_t)1));
		Card_t2092848 * L_11 = (__this->___cardModel_3);
		NullCheck(L_11);
		Sprite_t4006040370 * L_12 = (L_11->___cardBack_4);
		NullCheck(L_5);
		CardFlipper_FlipCard_m3019275047(L_5, (*(Sprite_t4006040370 **)(Sprite_t4006040370 **)SZArrayLdElema(L_7, L_10, sizeof(Sprite_t4006040370 *))), L_12, (-1), /*hidden argument*/NULL);
		goto IL_0102;
	}

IL_007e:
	{
		int32_t L_13 = (__this->___cardIndex_5);
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_00c6;
		}
	}
	{
		CardFlipper_t1465479232 * L_14 = (__this->___flipper_4);
		Card_t2092848 * L_15 = (__this->___cardModel_3);
		NullCheck(L_15);
		SpriteU5BU5D_t503173063* L_16 = (L_15->___faces_3);
		int32_t L_17 = (__this->___cardIndex_5);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, ((int32_t)((int32_t)L_17-(int32_t)1)));
		int32_t L_18 = ((int32_t)((int32_t)L_17-(int32_t)1));
		Card_t2092848 * L_19 = (__this->___cardModel_3);
		NullCheck(L_19);
		SpriteU5BU5D_t503173063* L_20 = (L_19->___faces_3);
		int32_t L_21 = (__this->___cardIndex_5);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		int32_t L_23 = (__this->___cardIndex_5);
		NullCheck(L_14);
		CardFlipper_FlipCard_m3019275047(L_14, (*(Sprite_t4006040370 **)(Sprite_t4006040370 **)SZArrayLdElema(L_16, L_18, sizeof(Sprite_t4006040370 *))), (*(Sprite_t4006040370 **)(Sprite_t4006040370 **)SZArrayLdElema(L_20, L_22, sizeof(Sprite_t4006040370 *))), L_23, /*hidden argument*/NULL);
		goto IL_00f4;
	}

IL_00c6:
	{
		CardFlipper_t1465479232 * L_24 = (__this->___flipper_4);
		Card_t2092848 * L_25 = (__this->___cardModel_3);
		NullCheck(L_25);
		Sprite_t4006040370 * L_26 = (L_25->___cardBack_4);
		Card_t2092848 * L_27 = (__this->___cardModel_3);
		NullCheck(L_27);
		SpriteU5BU5D_t503173063* L_28 = (L_27->___faces_3);
		int32_t L_29 = (__this->___cardIndex_5);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		int32_t L_31 = (__this->___cardIndex_5);
		NullCheck(L_24);
		CardFlipper_FlipCard_m3019275047(L_24, L_26, (*(Sprite_t4006040370 **)(Sprite_t4006040370 **)SZArrayLdElema(L_28, L_30, sizeof(Sprite_t4006040370 *))), L_31, /*hidden argument*/NULL);
	}

IL_00f4:
	{
		int32_t L_32 = (__this->___cardIndex_5);
		__this->___cardIndex_5 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_0102:
	{
		return;
	}
}
// System.Void DebugDealer::.ctor()
extern "C"  void DebugDealer__ctor_m4258999999 (DebugDealer_t3963452972 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DebugDealer::OnGUI()
extern TypeInfo* GUI_t1522956648_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2217798;
extern const uint32_t DebugDealer_OnGUI_m3754398649_MetadataUsageId;
extern "C"  void DebugDealer_OnGUI_m3754398649 (DebugDealer_t3963452972 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DebugDealer_OnGUI_m3754398649_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rect_t1525428817  L_0 = {0};
		Rect__ctor_m3291325233(&L_0, (10.0f), (10.0f), (100.0f), (28.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1522956648_il2cpp_TypeInfo_var);
		bool L_1 = GUI_Button_m885093907(NULL /*static, unused*/, L_0, _stringLiteral2217798, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003e;
		}
	}
	{
		CardStack_t1747837240 * L_2 = (__this->___player_2);
		CardStack_t1747837240 * L_3 = (__this->___dealer_3);
		NullCheck(L_3);
		int32_t L_4 = CardStack_Pop_m3559568496(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		CardStack_Push_m1325389084(L_2, L_4, /*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void Draggable::.ctor()
extern "C"  void Draggable__ctor_m2724512414 (Draggable_t1699487085 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Draggable::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppCodeGenString* _stringLiteral563742238;
extern const uint32_t Draggable_OnBeginDrag_m1528043620_MetadataUsageId;
extern "C"  void Draggable_OnBeginDrag_m1528043620 (Draggable_t1699487085 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Draggable_OnBeginDrag_m1528043620_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral563742238, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Draggable::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppCodeGenString* _stringLiteral2365428691;
extern const uint32_t Draggable_OnDrag_m362468613_MetadataUsageId;
extern "C"  void Draggable_OnDrag_m362468613 (Draggable_t1699487085 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Draggable_OnDrag_m362468613_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral2365428691, /*hidden argument*/NULL);
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		PointerEventData_t3205101634 * L_1 = ___eventData;
		NullCheck(L_1);
		Vector2_t3525329788  L_2 = PointerEventData_get_position_m2263123361(L_1, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Draggable::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern Il2CppCodeGenString* _stringLiteral2365428691;
extern const uint32_t Draggable_OnEndDrag_m415526642_MetadataUsageId;
extern "C"  void Draggable_OnEndDrag_m415526642 (Draggable_t1699487085 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Draggable_OnEndDrag_m415526642_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral2365428691, /*hidden argument*/NULL);
		Transform_t284553113 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		PointerEventData_t3205101634 * L_1 = ___eventData;
		NullCheck(L_1);
		Vector2_t3525329788  L_2 = PointerEventData_get_position_m2263123361(L_1, /*hidden argument*/NULL);
		Vector3_t3525329789  L_3 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m4168274701 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		__this->___dealersFirstCard_2 = (-1);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Hit()
extern "C"  void GameController_Hit_m135787902 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameController::Stick()
extern "C"  void GameController_Stick_m3122345147 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		Button_t990034267 * L_0 = (__this->___hitButton_6);
		NullCheck(L_0);
		Selectable_set_interactable_m2686686419(L_0, (bool)0, /*hidden argument*/NULL);
		Button_t990034267 * L_1 = (__this->___stickButton_7);
		NullCheck(L_1);
		Selectable_set_interactable_m2686686419(L_1, (bool)0, /*hidden argument*/NULL);
		Object_t * L_2 = GameController_DealersTurn_m2095979194(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::PlayAgain()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var;
extern const uint32_t GameController_PlayAgain_m3337304247_MetadataUsageId;
extern "C"  void GameController_PlayAgain_m3337304247 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameController_PlayAgain_m3337304247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Button_t990034267 * L_0 = (__this->___playAgainButton_8);
		NullCheck(L_0);
		Selectable_set_interactable_m2686686419(L_0, (bool)0, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_1 = (__this->___deck_4);
		NullCheck(L_1);
		CardStackView_t4019401725 * L_2 = Component_GetComponent_TisCardStackView_t4019401725_m2584442780(L_1, /*hidden argument*/Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var);
		NullCheck(L_2);
		CardStackView_Clear_m1074402873(L_2, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_3 = (__this->___player_5);
		NullCheck(L_3);
		CardStackView_t4019401725 * L_4 = Component_GetComponent_TisCardStackView_t4019401725_m2584442780(L_3, /*hidden argument*/Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var);
		NullCheck(L_4);
		CardStackView_Clear_m1074402873(L_4, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_5 = (__this->___dealer_3);
		NullCheck(L_5);
		CardStackView_t4019401725 * L_6 = Component_GetComponent_TisCardStackView_t4019401725_m2584442780(L_5, /*hidden argument*/Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var);
		NullCheck(L_6);
		CardStackView_Clear_m1074402873(L_6, /*hidden argument*/NULL);
		Text_t3286458198 * L_7 = (__this->___winnerText_9);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_8);
		CardStack_t1747837240 * L_9 = (__this->___deck_4);
		NullCheck(L_9);
		CardStack_CreateDeck_m726432310(L_9, /*hidden argument*/NULL);
		__this->___dealersFirstCard_2 = (-1);
		Button_t990034267 * L_10 = (__this->___hitButton_6);
		NullCheck(L_10);
		Selectable_set_interactable_m2686686419(L_10, (bool)1, /*hidden argument*/NULL);
		Button_t990034267 * L_11 = (__this->___stickButton_7);
		NullCheck(L_11);
		Selectable_set_interactable_m2686686419(L_11, (bool)1, /*hidden argument*/NULL);
		GameController_StartGame_m2751531423(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Awake()
extern "C"  void GameController_Awake_m110912624 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	{
		GameController_StartGame_m2751531423(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::StartGame()
extern "C"  void GameController_StartGame_m2751531423 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0027;
	}

IL_0007:
	{
		CardStack_t1747837240 * L_0 = (__this->___player_5);
		CardStack_t1747837240 * L_1 = (__this->___deck_4);
		NullCheck(L_1);
		int32_t L_2 = CardStack_Pop_m3559568496(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		CardStack_Push_m1325389084(L_0, L_2, /*hidden argument*/NULL);
		GameController_HitDealer_m689685431(__this, /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_0027:
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) < ((int32_t)2)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void GameController::HitDealer()
extern const MethodInfo* Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var;
extern const uint32_t GameController_HitDealer_m689685431_MetadataUsageId;
extern "C"  void GameController_HitDealer_m689685431 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameController_HitDealer_m689685431_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	CardStackView_t4019401725 * V_1 = {0};
	{
		CardStack_t1747837240 * L_0 = (__this->___deck_4);
		NullCheck(L_0);
		int32_t L_1 = CardStack_Pop_m3559568496(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___dealersFirstCard_2);
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = V_0;
		__this->___dealersFirstCard_2 = L_3;
	}

IL_001f:
	{
		CardStack_t1747837240 * L_4 = (__this->___dealer_3);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		CardStack_Push_m1325389084(L_4, L_5, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_6 = (__this->___dealer_3);
		NullCheck(L_6);
		int32_t L_7 = CardStack_get_CardCount_m2692224405(L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) > ((int32_t)2)))
		{
			goto IL_0050;
		}
	}
	{
		CardStack_t1747837240 * L_8 = (__this->___dealer_3);
		NullCheck(L_8);
		CardStackView_t4019401725 * L_9 = Component_GetComponent_TisCardStackView_t4019401725_m2584442780(L_8, /*hidden argument*/Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var);
		V_1 = L_9;
		CardStackView_t4019401725 * L_10 = V_1;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		CardStackView_Toggle_m4093617474(L_10, L_11, (bool)1, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Collections.IEnumerator GameController::DealersTurn()
extern TypeInfo* U3CDealersTurnU3Ec__Iterator2_t120626468_il2cpp_TypeInfo_var;
extern const uint32_t GameController_DealersTurn_m2095979194_MetadataUsageId;
extern "C"  Object_t * GameController_DealersTurn_m2095979194 (GameController_t2782302542 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GameController_DealersTurn_m2095979194_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CDealersTurnU3Ec__Iterator2_t120626468 * V_0 = {0};
	{
		U3CDealersTurnU3Ec__Iterator2_t120626468 * L_0 = (U3CDealersTurnU3Ec__Iterator2_t120626468 *)il2cpp_codegen_object_new(U3CDealersTurnU3Ec__Iterator2_t120626468_il2cpp_TypeInfo_var);
		U3CDealersTurnU3Ec__Iterator2__ctor_m1749842520(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDealersTurnU3Ec__Iterator2_t120626468 * L_1 = V_0;
		return L_1;
	}
}
// System.Void GameController/<DealersTurn>c__Iterator2::.ctor()
extern "C"  void U3CDealersTurnU3Ec__Iterator2__ctor_m1749842520 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameController/<DealersTurn>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Object_t * U3CDealersTurnU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3523125636 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object GameController/<DealersTurn>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C"  Object_t * U3CDealersTurnU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m1460415768 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean GameController/<DealersTurn>c__Iterator2::MoveNext()
extern TypeInfo* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern const uint32_t U3CDealersTurnU3Ec__Iterator2_MoveNext_m205371524_MetadataUsageId;
extern "C"  bool U3CDealersTurnU3Ec__Iterator2_MoveNext_m205371524 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDealersTurnU3Ec__Iterator2_MoveNext_m205371524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003d;
		}
	}
	{
		goto IL_0044;
	}

IL_0021:
	{
		WaitForSeconds_t1291133240 * L_2 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_2, (1.0f), /*hidden argument*/NULL);
		__this->___U24current_1 = L_2;
		__this->___U24PC_0 = 1;
		goto IL_0046;
	}

IL_003d:
	{
		__this->___U24PC_0 = (-1);
	}

IL_0044:
	{
		return (bool)0;
	}

IL_0046:
	{
		return (bool)1;
	}
	// Dead block : IL_0048: ldloc.1
}
// System.Void GameController/<DealersTurn>c__Iterator2::Dispose()
extern "C"  void U3CDealersTurnU3Ec__Iterator2_Dispose_m2164732053 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void GameController/<DealersTurn>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CDealersTurnU3Ec__Iterator2_Reset_m3691242757_MetadataUsageId;
extern "C"  void U3CDealersTurnU3Ec__Iterator2_Reset_m3691242757 (U3CDealersTurnU3Ec__Iterator2_t120626468 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDealersTurnU3Ec__Iterator2_Reset_m3691242757_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void LifeHud::.ctor()
extern "C"  void LifeHud__ctor_m1068625296 (LifeHud_t1834144827 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LifeHud::Awake()
extern TypeInfo* myGameController_t698827258_il2cpp_TypeInfo_var;
extern TypeInfo* UpdateLifeHudHandler_t793365432_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t3286458198_m1610753993_MethodInfo_var;
extern const MethodInfo* LifeHud_myGameController_UpdateLife_m4259040581_MethodInfo_var;
extern const uint32_t LifeHud_Awake_m1306230515_MetadataUsageId;
extern "C"  void LifeHud_Awake_m1306230515 (LifeHud_t1834144827 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LifeHud_Awake_m1306230515_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = Component_GetComponent_TisText_t3286458198_m1610753993(__this, /*hidden argument*/Component_GetComponent_TisText_t3286458198_m1610753993_MethodInfo_var);
		__this->___text_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(myGameController_t698827258_il2cpp_TypeInfo_var);
		myGameController_t698827258 * L_1 = ((myGameController_t698827258_StaticFields*)myGameController_t698827258_il2cpp_TypeInfo_var->static_fields)->___instance_7;
		IntPtr_t L_2 = { (void*)LifeHud_myGameController_UpdateLife_m4259040581_MethodInfo_var };
		UpdateLifeHudHandler_t793365432 * L_3 = (UpdateLifeHudHandler_t793365432 *)il2cpp_codegen_object_new(UpdateLifeHudHandler_t793365432_il2cpp_TypeInfo_var);
		UpdateLifeHudHandler__ctor_m3112192095(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		myGameController_add_UpdateLifeHud_m2507554683(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LifeHud::myGameController_UpdateLife(System.Object,UpdateLifeHudEventArgs)
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral72431780;
extern const uint32_t LifeHud_myGameController_UpdateLife_m4259040581_MetadataUsageId;
extern "C"  void LifeHud_myGameController_UpdateLife_m4259040581 (LifeHud_t1834144827 * __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LifeHud_myGameController_UpdateLife_m4259040581_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = (__this->___text_2);
		UpdateLifeHudEventArgs_t2181688197 * L_1 = ___e;
		NullCheck(L_1);
		int32_t L_2 = UpdateLifeHudEventArgs_get_Life_m1055854817(L_1, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral72431780, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		return;
	}
}
// System.Void myGameController::.ctor()
extern "C"  void myGameController__ctor_m3758063073 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myGameController::.cctor()
extern "C"  void myGameController__cctor_m53742060 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void myGameController::add_UpdateShieldHud(UpdateShieldHudHandler)
extern TypeInfo* UpdateShieldHudHandler_t3707709861_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_add_UpdateShieldHud_m3606511201_MetadataUsageId;
extern "C"  void myGameController_add_UpdateShieldHud_m3606511201 (myGameController_t698827258 * __this, UpdateShieldHudHandler_t3707709861 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_add_UpdateShieldHud_m3606511201_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UpdateShieldHudHandler_t3707709861 * L_0 = (__this->___UpdateShieldHud_8);
		UpdateShieldHudHandler_t3707709861 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___UpdateShieldHud_8 = ((UpdateShieldHudHandler_t3707709861 *)CastclassSealed(L_2, UpdateShieldHudHandler_t3707709861_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void myGameController::remove_UpdateShieldHud(UpdateShieldHudHandler)
extern TypeInfo* UpdateShieldHudHandler_t3707709861_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_remove_UpdateShieldHud_m2064041032_MetadataUsageId;
extern "C"  void myGameController_remove_UpdateShieldHud_m2064041032 (myGameController_t698827258 * __this, UpdateShieldHudHandler_t3707709861 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_remove_UpdateShieldHud_m2064041032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UpdateShieldHudHandler_t3707709861 * L_0 = (__this->___UpdateShieldHud_8);
		UpdateShieldHudHandler_t3707709861 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___UpdateShieldHud_8 = ((UpdateShieldHudHandler_t3707709861 *)CastclassSealed(L_2, UpdateShieldHudHandler_t3707709861_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void myGameController::add_UpdateLifeHud(UpdateLifeHudHandler)
extern TypeInfo* UpdateLifeHudHandler_t793365432_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_add_UpdateLifeHud_m2507554683_MetadataUsageId;
extern "C"  void myGameController_add_UpdateLifeHud_m2507554683 (myGameController_t698827258 * __this, UpdateLifeHudHandler_t793365432 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_add_UpdateLifeHud_m2507554683_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UpdateLifeHudHandler_t793365432 * L_0 = (__this->___UpdateLifeHud_9);
		UpdateLifeHudHandler_t793365432 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Combine_m1842362874(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___UpdateLifeHud_9 = ((UpdateLifeHudHandler_t793365432 *)CastclassSealed(L_2, UpdateLifeHudHandler_t793365432_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void myGameController::remove_UpdateLifeHud(UpdateLifeHudHandler)
extern TypeInfo* UpdateLifeHudHandler_t793365432_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_remove_UpdateLifeHud_m1823150306_MetadataUsageId;
extern "C"  void myGameController_remove_UpdateLifeHud_m1823150306 (myGameController_t698827258 * __this, UpdateLifeHudHandler_t793365432 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_remove_UpdateLifeHud_m1823150306_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UpdateLifeHudHandler_t793365432 * L_0 = (__this->___UpdateLifeHud_9);
		UpdateLifeHudHandler_t793365432 * L_1 = ___value;
		Delegate_t3660574010 * L_2 = Delegate_Remove_m3898886541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		__this->___UpdateLifeHud_9 = ((UpdateLifeHudHandler_t793365432 *)CastclassSealed(L_2, UpdateLifeHudHandler_t793365432_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Int32 myGameController::get_Life()
extern "C"  int32_t myGameController_get_Life_m3530338390 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CLifeU3Ek__BackingField_10);
		return L_0;
	}
}
// System.Void myGameController::set_Life(System.Int32)
extern "C"  void myGameController_set_Life_m1379915789 (myGameController_t698827258 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CLifeU3Ek__BackingField_10 = L_0;
		return;
	}
}
// System.Int32 myGameController::get_Shield()
extern "C"  int32_t myGameController_get_Shield_m2441234243 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CShieldU3Ek__BackingField_11);
		return L_0;
	}
}
// System.Void myGameController::set_Shield(System.Int32)
extern "C"  void myGameController_set_Shield_m565064186 (myGameController_t698827258 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CShieldU3Ek__BackingField_11 = L_0;
		return;
	}
}
// System.Void myGameController::Awake()
extern TypeInfo* myGameController_t698827258_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_Awake_m3995668292_MetadataUsageId;
extern "C"  void myGameController_Awake_m3995668292 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_Awake_m3995668292_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(myGameController_t698827258_il2cpp_TypeInfo_var);
		myGameController_t698827258 * L_0 = ((myGameController_t698827258_StaticFields*)myGameController_t698827258_il2cpp_TypeInfo_var->static_fields)->___instance_7;
		bool L_1 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_0, (Object_t3878351788 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(myGameController_t698827258_il2cpp_TypeInfo_var);
		((myGameController_t698827258_StaticFields*)myGameController_t698827258_il2cpp_TypeInfo_var->static_fields)->___instance_7 = __this;
		goto IL_0036;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(myGameController_t698827258_il2cpp_TypeInfo_var);
		myGameController_t698827258 * L_2 = ((myGameController_t698827258_StaticFields*)myGameController_t698827258_il2cpp_TypeInfo_var->static_fields)->___instance_7;
		bool L_3 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_2, __this, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void myGameController::Start()
extern "C"  void myGameController_Start_m2705200865 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	{
		myGameController_StartGame_m1288972915(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myGameController::StartGame()
extern TypeInfo* UpdateLifeHudEventArgs_t2181688197_il2cpp_TypeInfo_var;
extern TypeInfo* UpdateShieldHudEventArgs_t2548007474_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_StartGame_m1288972915_MetadataUsageId;
extern "C"  void myGameController_StartGame_m1288972915 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_StartGame_m1288972915_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		myGameController_DealCards_m3776010390(__this, /*hidden argument*/NULL);
		__this->___life_5 = ((int32_t)21);
		UpdateLifeHudHandler_t793365432 * L_0 = (__this->___UpdateLifeHud_9);
		int32_t L_1 = (__this->___life_5);
		UpdateLifeHudEventArgs_t2181688197 * L_2 = (UpdateLifeHudEventArgs_t2181688197 *)il2cpp_codegen_object_new(UpdateLifeHudEventArgs_t2181688197_il2cpp_TypeInfo_var);
		UpdateLifeHudEventArgs__ctor_m361874183(L_2, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		UpdateLifeHudHandler_Invoke_m411995846(L_0, __this, L_2, /*hidden argument*/NULL);
		UpdateShieldHudHandler_t3707709861 * L_3 = (__this->___UpdateShieldHud_8);
		if (!L_3)
		{
			goto IL_0047;
		}
	}
	{
		UpdateShieldHudHandler_t3707709861 * L_4 = (__this->___UpdateShieldHud_8);
		int32_t L_5 = (__this->___shield_6);
		UpdateShieldHudEventArgs_t2548007474 * L_6 = (UpdateShieldHudEventArgs_t2548007474 *)il2cpp_codegen_object_new(UpdateShieldHudEventArgs_t2548007474_il2cpp_TypeInfo_var);
		UpdateShieldHudEventArgs__ctor_m1570989178(L_6, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		UpdateShieldHudHandler_Invoke_m2445626150(L_4, __this, L_6, /*hidden argument*/NULL);
	}

IL_0047:
	{
		return;
	}
}
// System.Void myGameController::DealCards()
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_DealCards_m3776010390_MetadataUsageId;
extern "C"  void myGameController_DealCards_m3776010390 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_DealCards_m3776010390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CardStack_t1747837240 * L_0 = (__this->___dealer_3);
		NullCheck(L_0);
		int32_t L_1 = CardStack_get_CardCount_m2692224405(L_0, /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_2);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_4 = (__this->___dealer_3);
		NullCheck(L_4);
		int32_t L_5 = CardStack_get_CardCount_m2692224405(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0027;
		}
	}
	{
		return;
	}

IL_0027:
	{
		Object_t * L_6 = myGameController_DealerGivesCards_m3368865797(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2135303124(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator myGameController::DealerGivesCards()
extern TypeInfo* U3CDealerGivesCardsU3Ec__Iterator3_t78322666_il2cpp_TypeInfo_var;
extern const uint32_t myGameController_DealerGivesCards_m3368865797_MetadataUsageId;
extern "C"  Object_t * myGameController_DealerGivesCards_m3368865797 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_DealerGivesCards_m3368865797_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * V_0 = {0};
	{
		U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * L_0 = (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 *)il2cpp_codegen_object_new(U3CDealerGivesCardsU3Ec__Iterator3_t78322666_il2cpp_TypeInfo_var);
		U3CDealerGivesCardsU3Ec__Iterator3__ctor_m863128492(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_5 = __this;
		U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * L_2 = V_0;
		return L_2;
	}
}
// System.Void myGameController::HitCount(System.Int32,System.Int32)
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* UpdateLifeHudEventArgs_t2181688197_il2cpp_TypeInfo_var;
extern TypeInfo* UpdateShieldHudEventArgs_t2548007474_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1381430670;
extern Il2CppCodeGenString* _stringLiteral2275922754;
extern const uint32_t myGameController_HitCount_m689921927_MetadataUsageId;
extern "C"  void myGameController_HitCount_m689921927 (myGameController_t698827258 * __this, int32_t ___card, int32_t ___cardIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (myGameController_HitCount_m689921927_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = (__this->___life_5);
		int32_t L_1 = ___card;
		__this->___life_5 = ((int32_t)((int32_t)L_0-(int32_t)L_1));
		int32_t L_2 = ___card;
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		int32_t L_5 = ___cardIndex;
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m2809334143(NULL /*static, unused*/, L_4, _stringLiteral1381430670, L_7, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		CardStack_t1747837240 * L_9 = (__this->___dealer_3);
		int32_t L_10 = ___card;
		NullCheck(L_9);
		CardStack_RemoveCard_m3746222646(L_9, L_10, /*hidden argument*/NULL);
		int32_t L_11 = Random_Range_m75452833(NULL /*static, unused*/, 1, ((int32_t)22), /*hidden argument*/NULL);
		__this->___shield_6 = L_11;
		UpdateLifeHudHandler_t793365432 * L_12 = (__this->___UpdateLifeHud_9);
		if (!L_12)
		{
			goto IL_0065;
		}
	}
	{
		UpdateLifeHudHandler_t793365432 * L_13 = (__this->___UpdateLifeHud_9);
		int32_t L_14 = (__this->___life_5);
		UpdateLifeHudEventArgs_t2181688197 * L_15 = (UpdateLifeHudEventArgs_t2181688197 *)il2cpp_codegen_object_new(UpdateLifeHudEventArgs_t2181688197_il2cpp_TypeInfo_var);
		UpdateLifeHudEventArgs__ctor_m361874183(L_15, L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		UpdateLifeHudHandler_Invoke_m411995846(L_13, __this, L_15, /*hidden argument*/NULL);
	}

IL_0065:
	{
		UpdateShieldHudHandler_t3707709861 * L_16 = (__this->___UpdateShieldHud_8);
		if (!L_16)
		{
			goto IL_0087;
		}
	}
	{
		UpdateShieldHudHandler_t3707709861 * L_17 = (__this->___UpdateShieldHud_8);
		int32_t L_18 = (__this->___shield_6);
		UpdateShieldHudEventArgs_t2548007474 * L_19 = (UpdateShieldHudEventArgs_t2548007474 *)il2cpp_codegen_object_new(UpdateShieldHudEventArgs_t2548007474_il2cpp_TypeInfo_var);
		UpdateShieldHudEventArgs__ctor_m1570989178(L_19, L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		UpdateShieldHudHandler_Invoke_m2445626150(L_17, __this, L_19, /*hidden argument*/NULL);
	}

IL_0087:
	{
		int32_t L_20 = (__this->___life_5);
		if ((((int32_t)L_20) >= ((int32_t)1)))
		{
			goto IL_0099;
		}
	}
	{
		myGameController_RestartGame_m2232649440(__this, /*hidden argument*/NULL);
	}

IL_0099:
	{
		int32_t L_21 = (__this->___life_5);
		int32_t L_22 = L_21;
		Object_t * L_23 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_22);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_24 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2275922754, L_23, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myGameController::RestartGame()
extern "C"  void myGameController_RestartGame_m2232649440 (myGameController_t698827258 * __this, const MethodInfo* method)
{
	{
		myGameController_StartGame_m1288972915(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void myGameController/<DealerGivesCards>c__Iterator3::.ctor()
extern "C"  void U3CDealerGivesCardsU3Ec__Iterator3__ctor_m863128492 (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object myGameController/<DealerGivesCards>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Object_t * U3CDealerGivesCardsU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3129828646 (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object myGameController/<DealerGivesCards>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C"  Object_t * U3CDealerGivesCardsU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m3549066426 (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean myGameController/<DealerGivesCards>c__Iterator3::MoveNext()
extern TypeInfo* WaitForSeconds_t1291133240_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var;
extern const uint32_t U3CDealerGivesCardsU3Ec__Iterator3_MoveNext_m3792767688_MetadataUsageId;
extern "C"  bool U3CDealerGivesCardsU3Ec__Iterator3_MoveNext_m3792767688 (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDealerGivesCardsU3Ec__Iterator3_MoveNext_m3792767688_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ae;
		}
	}
	{
		goto IL_00cf;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = 0;
		goto IL_00bc;
	}

IL_002d:
	{
		myGameController_t698827258 * L_2 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_2);
		CardStack_t1747837240 * L_3 = (L_2->___deck_2);
		NullCheck(L_3);
		int32_t L_4 = CardStack_Pop_m3559568496(L_3, /*hidden argument*/NULL);
		__this->___U3CcardU3E__1_1 = L_4;
		myGameController_t698827258 * L_5 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_5);
		CardStack_t1747837240 * L_6 = (L_5->___dealer_3);
		int32_t L_7 = (__this->___U3CcardU3E__1_1);
		NullCheck(L_6);
		CardStack_Push_m1325389084(L_6, L_7, /*hidden argument*/NULL);
		myGameController_t698827258 * L_8 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_8);
		CardStack_t1747837240 * L_9 = (L_8->___dealer_3);
		NullCheck(L_9);
		CardStackView_t4019401725 * L_10 = Component_GetComponent_TisCardStackView_t4019401725_m2584442780(L_9, /*hidden argument*/Component_GetComponent_TisCardStackView_t4019401725_m2584442780_MethodInfo_var);
		__this->___U3CviewU3E__2_2 = L_10;
		CardStackView_t4019401725 * L_11 = (__this->___U3CviewU3E__2_2);
		int32_t L_12 = (__this->___U3CcardU3E__1_1);
		NullCheck(L_11);
		CardStackView_Toggle_m4093617474(L_11, L_12, (bool)1, /*hidden argument*/NULL);
		CardStackView_t4019401725 * L_13 = (__this->___U3CviewU3E__2_2);
		int32_t L_14 = (__this->___U3CcardU3E__1_1);
		NullCheck(L_13);
		CardStackView_Flipper_m1991742797(L_13, L_14, /*hidden argument*/NULL);
		WaitForSeconds_t1291133240 * L_15 = (WaitForSeconds_t1291133240 *)il2cpp_codegen_object_new(WaitForSeconds_t1291133240_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_15, (1.0f), /*hidden argument*/NULL);
		__this->___U24current_4 = L_15;
		__this->___U24PC_3 = 1;
		goto IL_00d1;
	}

IL_00ae:
	{
		int32_t L_16 = (__this->___U3CiU3E__0_0);
		__this->___U3CiU3E__0_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_00bc:
	{
		int32_t L_17 = (__this->___U3CiU3E__0_0);
		if ((((int32_t)L_17) < ((int32_t)4)))
		{
			goto IL_002d;
		}
	}
	{
		__this->___U24PC_3 = (-1);
	}

IL_00cf:
	{
		return (bool)0;
	}

IL_00d1:
	{
		return (bool)1;
	}
	// Dead block : IL_00d3: ldloc.1
}
// System.Void myGameController/<DealerGivesCards>c__Iterator3::Dispose()
extern "C"  void U3CDealerGivesCardsU3Ec__Iterator3_Dispose_m436075753 (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void myGameController/<DealerGivesCards>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t1374155497_il2cpp_TypeInfo_var;
extern const uint32_t U3CDealerGivesCardsU3Ec__Iterator3_Reset_m2804528729_MetadataUsageId;
extern "C"  void U3CDealerGivesCardsU3Ec__Iterator3_Reset_m2804528729 (U3CDealerGivesCardsU3Ec__Iterator3_t78322666 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CDealerGivesCardsU3Ec__Iterator3_Reset_m2804528729_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1374155497 * L_0 = (NotSupportedException_t1374155497 *)il2cpp_codegen_object_new(NotSupportedException_t1374155497_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m149930845(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Pickable::.ctor()
extern "C"  void Pickable__ctor_m1829981152 (Pickable_t3620569371 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Pickable::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* myGameController_t698827258_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2330535431;
extern Il2CppCodeGenString* _stringLiteral1878645810;
extern const uint32_t Pickable_OnPointerClick_m3874987344_MetadataUsageId;
extern "C"  void Pickable_OnPointerClick_m3874987344 (Pickable_t3620569371 * __this, PointerEventData_t3205101634 * ___eventData, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Pickable_OnPointerClick_m3874987344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, _stringLiteral2330535431, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(myGameController_t698827258_il2cpp_TypeInfo_var);
		myGameController_t698827258 * L_0 = ((myGameController_t698827258_StaticFields*)myGameController_t698827258_il2cpp_TypeInfo_var->static_fields)->___instance_7;
		GameObject_t4012695102 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Card_t2092848 * L_2 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_1, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		NullCheck(L_2);
		int32_t L_3 = Card_get_CardValue_m3125440979(L_2, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_4 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Card_t2092848 * L_5 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_4, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		NullCheck(L_5);
		int32_t L_6 = (L_5->___cardIndex_5);
		NullCheck(L_0);
		myGameController_HitCount_m689921927(L_0, L_3, L_6, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_7 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Card_t2092848 * L_8 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_7, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = Card_get_CardValue_m3125440979(L_8, /*hidden argument*/NULL);
		int32_t L_10 = L_9;
		Object_t * L_11 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_10);
		GameObject_t4012695102 * L_12 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Card_t2092848 * L_13 = GameObject_GetComponent_TisCard_t2092848_m3913226925(L_12, /*hidden argument*/GameObject_GetComponent_TisCard_t2092848_m3913226925_MethodInfo_var);
		NullCheck(L_13);
		int32_t L_14 = (L_13->___cardIndex_5);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_15);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Concat_m2809334143(NULL /*static, unused*/, L_11, _stringLiteral1878645810, L_16, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_17, /*hidden argument*/NULL);
		GameObject_t4012695102 * L_18 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScreenHelper::.ctor()
extern "C"  void ScreenHelper__ctor_m4198074497 (ScreenHelper_t2451640154 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShieldHud::.ctor()
extern "C"  void ShieldHud__ctor_m230326397 (ShieldHud_t2861725870 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShieldHud::Awake()
extern TypeInfo* myGameController_t698827258_il2cpp_TypeInfo_var;
extern TypeInfo* UpdateShieldHudHandler_t3707709861_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisText_t3286458198_m1610753993_MethodInfo_var;
extern const MethodInfo* ShieldHud_myGameController_UpdateShield_m104902354_MethodInfo_var;
extern const uint32_t ShieldHud_Awake_m467931616_MetadataUsageId;
extern "C"  void ShieldHud_Awake_m467931616 (ShieldHud_t2861725870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShieldHud_Awake_m467931616_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = Component_GetComponent_TisText_t3286458198_m1610753993(__this, /*hidden argument*/Component_GetComponent_TisText_t3286458198_m1610753993_MethodInfo_var);
		__this->___text_2 = L_0;
		IL2CPP_RUNTIME_CLASS_INIT(myGameController_t698827258_il2cpp_TypeInfo_var);
		myGameController_t698827258 * L_1 = ((myGameController_t698827258_StaticFields*)myGameController_t698827258_il2cpp_TypeInfo_var->static_fields)->___instance_7;
		IntPtr_t L_2 = { (void*)ShieldHud_myGameController_UpdateShield_m104902354_MethodInfo_var };
		UpdateShieldHudHandler_t3707709861 * L_3 = (UpdateShieldHudHandler_t3707709861 *)il2cpp_codegen_object_new(UpdateShieldHudHandler_t3707709861_il2cpp_TypeInfo_var);
		UpdateShieldHudHandler__ctor_m3346913740(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		myGameController_add_UpdateShieldHud_m3606511201(L_1, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ShieldHud::myGameController_UpdateShield(System.Object,UpdateShieldHudEventArgs)
extern TypeInfo* Int32_t2847414787_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2779208151;
extern const uint32_t ShieldHud_myGameController_UpdateShield_m104902354_MetadataUsageId;
extern "C"  void ShieldHud_myGameController_UpdateShield_m104902354 (ShieldHud_t2861725870 * __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ShieldHud_myGameController_UpdateShield_m104902354_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Text_t3286458198 * L_0 = (__this->___text_2);
		UpdateShieldHudEventArgs_t2548007474 * L_1 = ___e;
		NullCheck(L_1);
		int32_t L_2 = UpdateShieldHudEventArgs_get_Shield_m1869541243(L_1, /*hidden argument*/NULL);
		int32_t L_3 = L_2;
		Object_t * L_4 = Box(Int32_t2847414787_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2779208151, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_5);
		return;
	}
}
// System.Void UpdateLifeHudEventArgs::.ctor(System.Int32)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t UpdateLifeHudEventArgs__ctor_m361874183_MetadataUsageId;
extern "C"  void UpdateLifeHudEventArgs__ctor_m361874183 (UpdateLifeHudEventArgs_t2181688197 * __this, int32_t ___life, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UpdateLifeHudEventArgs__ctor_m361874183_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___life;
		UpdateLifeHudEventArgs_set_Life_m239895192(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UpdateLifeHudEventArgs::get_Life()
extern "C"  int32_t UpdateLifeHudEventArgs_get_Life_m1055854817 (UpdateLifeHudEventArgs_t2181688197 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CLifeU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UpdateLifeHudEventArgs::set_Life(System.Int32)
extern "C"  void UpdateLifeHudEventArgs_set_Life_m239895192 (UpdateLifeHudEventArgs_t2181688197 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CLifeU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Void UpdateLifeHudHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateLifeHudHandler__ctor_m3112192095 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UpdateLifeHudHandler::Invoke(System.Object,UpdateLifeHudEventArgs)
extern "C"  void UpdateLifeHudHandler_Invoke_m411995846 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UpdateLifeHudHandler_Invoke_m411995846((UpdateLifeHudHandler_t793365432 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, void* __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,il2cpp_codegen_get_delegate_this(__this),___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(il2cpp_codegen_get_delegate_this(__this),___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UpdateLifeHudEventArgs_t2181688197 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UpdateLifeHudHandler_t793365432(Il2CppObject* delegate, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult UpdateLifeHudHandler::BeginInvoke(System.Object,UpdateLifeHudEventArgs,System.AsyncCallback,System.Object)
extern "C"  Object_t * UpdateLifeHudHandler_BeginInvoke_m3918480583 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___sender, UpdateLifeHudEventArgs_t2181688197 * ___e, AsyncCallback_t1363551830 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UpdateLifeHudHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateLifeHudHandler_EndInvoke_m550289903 (UpdateLifeHudHandler_t793365432 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UpdateShieldHudEventArgs::.ctor(System.Int32)
extern TypeInfo* EventArgs_t516466188_il2cpp_TypeInfo_var;
extern const uint32_t UpdateShieldHudEventArgs__ctor_m1570989178_MetadataUsageId;
extern "C"  void UpdateShieldHudEventArgs__ctor_m1570989178 (UpdateShieldHudEventArgs_t2548007474 * __this, int32_t ___shield, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (UpdateShieldHudEventArgs__ctor_m1570989178_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(EventArgs_t516466188_il2cpp_TypeInfo_var);
		EventArgs__ctor_m1904770202(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___shield;
		UpdateShieldHudEventArgs_set_Shield_m2822943794(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UpdateShieldHudEventArgs::get_Shield()
extern "C"  int32_t UpdateShieldHudEventArgs_get_Shield_m1869541243 (UpdateShieldHudEventArgs_t2548007474 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CShieldU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UpdateShieldHudEventArgs::set_Shield(System.Int32)
extern "C"  void UpdateShieldHudEventArgs_set_Shield_m2822943794 (UpdateShieldHudEventArgs_t2548007474 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CShieldU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Void UpdateShieldHudHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateShieldHudHandler__ctor_m3346913740 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UpdateShieldHudHandler::Invoke(System.Object,UpdateShieldHudEventArgs)
extern "C"  void UpdateShieldHudHandler_Invoke_m2445626150 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		UpdateShieldHudHandler_Invoke_m2445626150((UpdateShieldHudHandler_t3707709861 *)__this->___prev_9,___sender, ___e, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, void* __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,il2cpp_codegen_get_delegate_this(__this),___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(il2cpp_codegen_get_delegate_this(__this),___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, UpdateShieldHudEventArgs_t2548007474 * ___e, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___sender, ___e,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_UpdateShieldHudHandler_t3707709861(Il2CppObject* delegate, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e)
{
	// Marshaling of parameter '___sender' to native representation
	Object_t * ____sender_marshaled = { 0 };
	IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult UpdateShieldHudHandler::BeginInvoke(System.Object,UpdateShieldHudEventArgs,System.AsyncCallback,System.Object)
extern "C"  Object_t * UpdateShieldHudHandler_BeginInvoke_m1000213153 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___sender, UpdateShieldHudEventArgs_t2548007474 * ___e, AsyncCallback_t1363551830 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___sender;
	__d_args[1] = ___e;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UpdateShieldHudHandler::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateShieldHudHandler_EndInvoke_m1224610268 (UpdateShieldHudHandler_t3707709861 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
