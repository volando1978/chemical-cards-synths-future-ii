﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1094945144MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2823629379(__this, ___dictionary, method) ((  void (*) (Enumerator_t314299514 *, Dictionary_2_t547271572 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m175752958(__this, method) ((  Object_t * (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1993024146(__this, method) ((  void (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3995248731(__this, method) ((  DictionaryEntry_t130027246  (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3884666202(__this, method) ((  Object_t * (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1886076268(__this, method) ((  Object_t * (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::MoveNext()
#define Enumerator_MoveNext_m3557014782(__this, method) ((  bool (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::get_Current()
#define Enumerator_get_Current_m2290329586(__this, method) ((  KeyValuePair_2_t35802870  (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4142271435(__this, method) ((  int32_t (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1729814319(__this, method) ((  CardView_t56460789 * (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::Reset()
#define Enumerator_Reset_m3272413205(__this, method) ((  void (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::VerifyState()
#define Enumerator_VerifyState_m1912028254(__this, method) ((  void (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1958021190(__this, method) ((  void (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,CardView>::Dispose()
#define Enumerator_Dispose_m3396458405(__this, method) ((  void (*) (Enumerator_t314299514 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
