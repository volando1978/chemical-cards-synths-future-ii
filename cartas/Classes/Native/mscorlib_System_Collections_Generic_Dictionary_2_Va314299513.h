﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType4014882752.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En314299513.h"

// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>
struct  Enumerator_t314299513 
{
	// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator::host_enumerator
	Enumerator_t314299514  ___host_enumerator_0;
};
