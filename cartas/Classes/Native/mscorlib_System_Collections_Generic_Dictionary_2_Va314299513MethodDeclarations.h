﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1094945144MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m982525827(__this, ___host, method) ((  void (*) (Enumerator_t314299513 *, Dictionary_2_t547271572 *, const MethodInfo*))Enumerator__ctor_m1006186640_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2436938686(__this, method) ((  Object_t * (*) (Enumerator_t314299513 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2834115931_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2243714386(__this, method) ((  void (*) (Enumerator_t314299513 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2279155237_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>::Dispose()
#define Enumerator_Dispose_m219849957(__this, method) ((  void (*) (Enumerator_t314299513 *, const MethodInfo*))Enumerator_Dispose_m2797419314_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>::MoveNext()
#define Enumerator_MoveNext_m1611801909(__this, method) ((  bool (*) (Enumerator_t314299513 *, const MethodInfo*))Enumerator_MoveNext_m3538465109_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,CardView>::get_Current()
#define Enumerator_get_Current_m2226557018(__this, method) ((  CardView_t56460789 * (*) (Enumerator_t314299513 *, const MethodInfo*))Enumerator_get_Current_m2952798389_gshared)(__this, method)
