﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CardView
struct CardView_t56460789;

#include "mscorlib_System_ValueType4014882752.h"

// System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>
struct  KeyValuePair_2_t35802870 
{
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int32_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	CardView_t56460789 * ___value_1;
};
