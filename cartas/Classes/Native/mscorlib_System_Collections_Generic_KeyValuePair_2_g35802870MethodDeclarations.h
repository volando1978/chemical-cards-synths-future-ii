﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_816448501MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m958327325(__this, ___key, ___value, method) ((  void (*) (KeyValuePair_2_t35802870 *, int32_t, CardView_t56460789 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>::get_Key()
#define KeyValuePair_2_get_Key_m3017876651(__this, method) ((  int32_t (*) (KeyValuePair_2_t35802870 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m681824748(__this, ___value, method) ((  void (*) (KeyValuePair_2_t35802870 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>::get_Value()
#define KeyValuePair_2_get_Value_m3471877135(__this, method) ((  CardView_t56460789 * (*) (KeyValuePair_2_t35802870 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2712424428(__this, ___value, method) ((  void (*) (KeyValuePair_2_t35802870 *, CardView_t56460789 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,CardView>::ToString()
#define KeyValuePair_2_ToString_m971781852(__this, method) ((  String_t* (*) (KeyValuePair_2_t35802870 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
